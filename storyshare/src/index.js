import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import { Provider } from 'react-redux'
import {  BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { store } from './store'
import { URL } from './constants'

import App from './components/App';
import Home from './components/Home';
import Users from './components/Users';
import Login from './components/Login';
import Registration from './components/Registration';
import VideoPlayerView from './components/VideoPlayerView';
import Profile from './components/Profile'
import AuthComponent from './components/HOC'
import Share from './components/Share'
import CreatePost from './components/CreatePost'
import MyPosts from './components/MyPosts'
// import CreateRespond from './components/CreateRespond'

import 'bulma/css/bulma.css'

// import { refreshAction } from "./store/actions/loginAction";
// import jwtDecode from "jwt-decode";
import { login } from "./store/actions/loginAction";
// import AuthComponent from "./HOC";
console.log("PROCESS.ENV:", process.env);
console.log("URL", URL);

const token = localStorage.getItem("token");
if (token) {
  store.dispatch(login(token));
}

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <Switch>
                <App>
                    <Route  path='/' exact component={ Home } />
                    <Route  path='/login' exact component={ Login } />
                    <Route  path='/users' exact component={ Users } />
                    <Route  path='/myposts' exact component={ MyPosts } />
                    <Route  path="/me" exact component={AuthComponent(Profile)}/>
                    <Route  path='/register' exact component={ Registration } />
                    <Route  path='/cp' exact component={CreatePost} />
                    <Route  path='/play/:post_id' component={ VideoPlayerView } />
                    <Route path='/share' component={Share} />
                    {/* <Route path='/ee' exact component={CreateRespond} /> */}
                </App>
            </Switch>
        </Router>
    </Provider>
, document.getElementById('root'));

// export function checker() {
//     try {
//       let decoded = jwtDecode(token);
//       let dateNow = new Date();
//       return decoded.exp < dateNow.getTime();
//     } catch (error) {
//       console.log(error);
//     }
//   }

//   function Redirecter(match) {
//     console.log(checker());
//     if (checker()) {
//       return (
//         <div>
//           <Route component={AuthComponent(Home)} />
//           <Route exact path={match.path} />
//         </div>
//       );
//     } else {
//       console.log("!token");
//       return refreshAction(token);
//     }
//   }

