/**
 * use this to transform redux data stored as object into an array
 * It reverses the ArrToObj function
 */
export const objToArr = (obj) => Object.values(obj).map(item => item);

/**
 * This transforms arrays of objects into a single object with the id as the key (a hashtable)
 * It expects the objects into the array to have a id property
 */
export const arrToObj = (arr) => arr.reduce((acc, obj) => {
    acc[obj.id] = obj;
    return acc;
}, {});



/**
 * This takes all keys of a hashtable and returns a numerically sorted array of keys (largest to smallest)
 * Which can be used to display items in a certain order (in this case to display newer posts first)
 */
export const getSortedIDs = hashtable => Object.keys(hashtable).sort((a, b) => b-a);


/**
 * use this to transform redux data stored as object into an array
 * @param {Object} obj an object where the key is the id of the post (essentially a hashtable)
 * @param {Array} listOfIds an array of ids used to specify which keys of the hashmap to include in the returned array
 */
export const objToArrOrdered = (obj, listOfIds) => {
    // intentionally not using functional style for performance reasons
    const length = listOfIds.length;
    const arr = [];
    for (let i = 0; i < length; i++ ) {
        const currentId = listOfIds[i];

        // check if currentId exists and push the value
        const current = obj[currentId];
        if (current) {
            arr.push(current);
        }
    }
    return arr;
};
