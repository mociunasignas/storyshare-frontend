import React, { useState } from 'react';
import ShareURL from '../../ShareURL'
import ShareEmbeded from '../../ShareEmbeded'

const Modal = ({ children, setToggleModal, modalState, title }) => {
  

  const [CARD, setCARD] = useState(<ShareURL />);
  const [bottonStyleUrl, setBottonStyleUrl] = useState('button is-text')
  const [bottonStyleEmbed, setBottonStyleEmbed] = useState('button is-white')
    
    if(!modalState) {
      return null;
    }

    const switcher = (e) => {
      switch (e) {
          case 'url':
              setCARD(<ShareURL />)
              setBottonStyleUrl('button is-white')
              break;
          case 'embed':
              setCARD(<ShareEmbeded />)
              setBottonStyleEmbed('button is-text')
              break;

          default:
              console.log(e)
              break;
      }
  }


    
    
    return(
          <section className="modal-card-body">
            <div className="content">
            {CARD}
            <button
                  class={bottonStyleUrl}
                  onClick={(e) => switcher(e.target.value)}
                  value='url'>
                  URL
              </button>

              <button
                  class={bottonStyleEmbed}
                  onClick={(e) => switcher(e.target.value)}
                  value='embed'>
                  EMBED
              </button>
          
            </div>
          </section>
    );
  }

export default Modal