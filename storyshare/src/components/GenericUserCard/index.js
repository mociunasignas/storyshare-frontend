import React from 'react';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom'
import './index.scss';
import {followUserAction} from '../../store/actions/followUserAction'
import {followingAction} from '../../store/actions/followingAction'
import {followersAction} from '../../store/actions/followersAction'
import {getUsers} from "../../store/actions/userAction";
import {myFeedAction} from "../../store/actions/myFeedAction";

// TODO add custom video poster
const GenericUserCard = ({user, dispatch}) => {

    const handleFollow = async () => {
        await dispatch(followUserAction(user.id));
        await dispatch(followingAction());
        dispatch(myFeedAction());
        dispatch(followersAction());
        dispatch(getUsers())
    };

    return (
        <div className='column is-one-third-tablet is-one-third-desktop'>
            <div className=' generic-post-card card '>

                <div className='section profile-heading' id='profile-heading'>
                    <div className='title is-bold'>
                        {user.first_name} {user.last_name}
                    </div>
                    <div className=' columns is-mobile is-multiline'>
                        <div className='column is-4'>
                        <span className='header-icon user-profile-image'>
                            <img
                                src={"https://html.com/wp-content/uploads/flamingo.jpg"}
                                alt={"randomAltProp"}
                            />
                        </span>
                            <button
                                onClick={handleFollow}
                                className='button is-outlined is-primary'
                            >Follow
                            </button>
                        </div>
                        <div className='column is-8 is-multiline no-padding'>

                            <div className='level has-text-centered'>
                                <div className='column is-one-third has-text-centered no-padding'>
                                    <p className='stat-key is-size-3 has-text-weight-bold'>{user.post_count}</p>
                                    <p className='stat-key is-size-6'>posts</p>
                                </div>

                                <div className='column is-one-third has-text-centered no-padding'>
                                    <p className='stat-key is-size-3 has-text-weight-bold'>{user.followee_count}</p>
                                    <p className='stat-key is-size-6'>following</p>
                                </div>

                                <div className='column is-one-third has-text-centered no-padding'>
                                    <p className='stat-key is-size-3 has-text-weight-bold'>{user.follower_count}</p>
                                    <p className='stat-key is-size-6'>followers</p>
                                </div>
                            </div>
                        </div>
                        <div className='column'>
                            White is actually one of my favorite colors. I have a white car. I love white.
                        </div>
                    </div>
                    <div className='has-text-weight-bold'>Latest posts</div>
                    <div>
                        <img className='generic-user-qustion-video'
                             src={"https://images.pexels.com/photos/406014/pexels-photo-406014.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500"}
                             alt={"randomAltProp"}/>
                        <div>6 days ago</div>
                    </div>
                </div>
            </div>
        </div>
    )
};

const mapStateToProps = (state) => {
    // const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    return {
        // search: search
    }
};

export default withRouter(connect(mapStateToProps)(GenericUserCard));
