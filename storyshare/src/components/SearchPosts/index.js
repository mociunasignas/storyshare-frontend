import React, {Component} from 'react';
import {connect} from 'react-redux';
import GenericPostList from "../GenericPostList";
import './index.scss';

class SearchPosts extends Component {

    render() {
        if (this.props.search) {
            return (
                <div className='latest-posts'>
                    <GenericPostList posts={this.props.search_posts}/>
                </div>
            )
        } else {
            return (
                <div>no matching results found, sorry!</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    const search_posts = state.searchPostReducer.search_posts ? state.searchPostReducer.search_posts : null;
    return {
        search: search,
        search_posts: search_posts
    }
};

export default connect(mapStateToProps)(SearchPosts);
