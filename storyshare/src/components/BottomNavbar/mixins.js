import {Link} from 'react-router-dom';
import React from "react";


export const HomeButton =  () => (
    <Link to='/'>
        <i className={'fas fa-thumbs-up'}> </i>
        {/*<div>Home</div>*/}
    </Link>
);