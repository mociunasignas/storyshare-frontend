import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import './index.scss';


const BottomNavbar = (props) => {
    // specify all tabs that will be visible in the Header
    // TODO make it work with JSX not only text
    // const navLinks = [
    //     {display: 'Feed', url: '/'},
    //     {display: 'Users', url: '/users'},
    //     {display: 'New', url: '/new'},
    //     {display: 'search', url: '/search'},
    //     {display: 'Profile', url: '/me'}
    // ];

    // find out which Link is active (for css highlighting reasons)
    // const getActiveTab = () => {
    //     const path = props.location.pathname;
    //     return navLinks.findIndex(el => el.url === path);
    // };

    // let activeTab = getActiveTab();
    return (
        <nav className='bottom-nav is-hidden-tablet'>
            <Link to='/'><i className="fas fa-home"> </i></Link>
            <Link to='/users'><i className={'fas fa-users'}> </i></Link>
            <Link to='/create'><i className={'fas fa-plus-square'}> </i></Link>
            <Link to='/search'><i className={'fas fa-search'}> </i></Link>
            <Link to='/me'><i className={'fas fa-user'}> </i></Link>
        </nav>
    );
};

export default withRouter(BottomNavbar);