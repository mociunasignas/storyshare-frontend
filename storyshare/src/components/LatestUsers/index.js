import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import './index.scss';
import GenericUsersList from "../GenericUsersList";
import {objToArr} from '../../helpers'
import {getUsers} from '../../store/actions/userAction'
import {getFollowers} from '../../store/actions/followersAction'
import {getFollowing} from '../../store/actions/followingAction'

// TODO this component should always make a fetch and display latest posts returned, (bonus: fetch on interval when rendered)
const LatestUsers = ({users, followers, following, dispatch}) => {
    useEffect(() => {
            dispatch(getUsers());
            dispatch(getFollowers());
            dispatch(getFollowing());
            // eslint-disable-next-line
      }, []);

    return (
        <div className='latest-users'>
            <GenericUsersList users={users} followers={followers} following={following}/>
        </div>
    )
};

const mapStateToProps = (state, ownProps) => {
    return {
        'users': objToArr(state.userReducer),
        'followers': objToArr(state.followersReducer),
        'following': objToArr(state.followingReducer),
    }
};

export default connect(mapStateToProps)(LatestUsers);
