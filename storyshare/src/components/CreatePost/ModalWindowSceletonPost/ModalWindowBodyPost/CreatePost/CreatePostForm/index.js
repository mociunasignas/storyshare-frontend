import React, { useState, useEffect } from 'react';
import { connect } from 'react-redux';
import { getCategories } from '../../../../../../store/actions/categoryAction'
import { objToArr } from '../../../../../../helpers'
import Tags from './Tags'


const CreatePostForm = ({
  categories, 
  tags, 
  setTags, 
  tagsInput, 
  setTagsInput, 
  setOptionID, 
  dispatch,
  title,
  setTitle,}) => {


  useEffect(() => {
    dispatch(getCategories());
    // eslint-disable-next-line
  }, []);


  let Flag
  const [option, setOption] = useState('cat')
  const [isOpen, setIsOpen] = useState('dropdown is-large')
  const [openFlag, setOpenFlag] = useState(true)



  const setOpenDropDown = () => {
    if (openFlag) {
      setOpenFlag(false)
      setIsOpen('dropdown is-active')
    } else {
      setOpenFlag(true)
      setIsOpen('dropdown')
    }
  }
  
  if (categories !== undefined) {
    // eslint-disable-next-line
    const a = categories
  }
  if (categories && Flag) {
    setOption(categories[0].category_name)
    Flag = false
  }
  const changeMenu = (element) => {
    setOption(element.category_name)
    setOptionID(element.id)
    setOpenFlag(true)
    setIsOpen('dropdown')
  }

  const submitHandlerTags = (e) => {
    e.preventDefault()
    const tagsArray = tags
    const cross = '  ❌'
    const hastag = '#'
    const res = tagsInput.concat(cross)
    const finres = hastag.concat(res)
    tagsArray.push(finres)
    setTags(tagsArray)
    setTagsInput('')
  }

  const tagDeleter = (e) => {
    e.preventDefault()
    const id = (e.target.value)
    const tagArray = tags
    tagArray.splice(id, 1)
    setTags([...tagArray])
  }

  return (
    (categories[0] && (
      <div>
        <div style={{ height: "100%", display: "flex", flexDirection: "column" }} id="superSection" className={isOpen}>
          <div className="dropdown-trigger" onClick={() => setOpenDropDown()}>
            <button style={{ width: "100%" }} className="button" aria-haspopup="true" aria-controls="dropdown-menu"  >
              <span>{option}</span>
              <span className="icon is-small">
                <i className="fas fa-angle-down" aria-hidden="true"></i>
              </span>
            </button>


            <div className="dropdown-menu" id="dropdown-menu" role="menu">
              <div className="dropdown-content">
                {categories[0].map((element, id) =>
                // eslint-disable-next-line
                  <a style={{ width: "100vw" }} key={id} value={element.id} className="dropdown-item" onClick={(e) => changeMenu(element)}>
                    {element.category_name}
                  </a>
                )}
              </div>
            </div>
          </div>
        </div>

        <textarea className="textarea" value={title} onChange={(e) => setTitle(e.target.value)} placeholder="Title" rows='2'></textarea>

        <form onSubmit={(e) => submitHandlerTags(e)}>
          <textarea value={tagsInput} onChange={(e) => setTagsInput(e.target.value)} ></textarea>
          <button onClick={(e) => submitHandlerTags(e)}>add</button>
          <Tags tags={tags} tagDeleter={(e) => tagDeleter(e)} />
        </form>
      </div>
    )) || null
  );
}

const mapStateToProps = (state, ownProps) => {
  return {
    'categories': objToArr(state.categoryReducer),
    'token': state.loginReducer.token,
  }
};


export default connect(mapStateToProps)(CreatePostForm);