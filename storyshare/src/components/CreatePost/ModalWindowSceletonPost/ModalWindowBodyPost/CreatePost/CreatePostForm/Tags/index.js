import React from 'react';

const Tags = ({ tags, tagDeleter }) => {

    
    return (
        <div>
            {tags.map((tagElement, ElementID) => {
                return <button key={ElementID} value={ElementID} onClick={(e) => tagDeleter(e)}>{tagElement}</button>
            })}
            {/* {console.log('ddd', tags)} */}
        </div>
    )
}

export default Tags