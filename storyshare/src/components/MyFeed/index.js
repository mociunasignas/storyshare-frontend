import React, {Component} from 'react';
import {connect} from 'react-redux';
import GenericPostList from "../GenericPostList";
// import './index.scss';

class MyFeed extends Component {

    render() {
        if (this.props.my_feed) {
            return (
                <GenericPostList posts={this.props.my_feed}/>
            )
        } else {
            return (
                <div>hello</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const my_feed = state.myFeedReducer.my_feed ? state.myFeedReducer.my_feed : null;

    return {
        my_feed: my_feed
    }
};

export default connect(mapStateToProps)(MyFeed);
