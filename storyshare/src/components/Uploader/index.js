import React, { useState } from 'react';
import { connect } from 'react-redux';
import axios from 'axios';
import uuid from "uuid";
import { URL } from '../../constants';
import './index.scss';

const BottonStyle = {
  color: 'blue',
  // backgroundImage: 'url(' + imgUrl + ')',
};



const Uploader = (props, { history }) => {


  //     useEffect(() => {
  //         dispatch(getUsers());
  //         // dispatch(getFollowers());
  //         // dispatch(getFollowing());
  //   }, []);


  const [video, setVideo] = useState(null)

  const handleSubmit = (e) => {
    e.preventDefault();
    const video_uuid = uuid.v4()
    console.log(props.token)
    let form_data = new FormData();
    form_data.append('file', video, video_uuid);
    form_data.append('video_uuid', video_uuid);
    form_data.append('thumbnail', video, video_uuid);
    let url = `${URL}upload/`;
    axios.post(url, form_data, {
      headers: {
        Authorization: `Bearer ${props.token}`,
        'content-type': 'multipart/form-data'
      }
    }).then(
      props.history.push(`/cp/${video_uuid}`)
    ).catch(err => console.log(err))

  }


  return (
    <div className="App">
      <form onSubmit={(e) => handleSubmit(e)}>
        <p>
          <input type="file"
            id="video"
            onChange={e => setVideo(e.target.files[0])} required />
        </p>
        <input type="submit" />
      </form>

      <div className="card">
        <div className="card-image">
          <figure className="image is-4by3">
            <img src="https://bulma.io/images/placeholders/1280x960.png" alt="Placeholder image" />
          </figure>
        </div>
          <div className="card-content">
            <div className="media">
        
            </div>

            <div className="content">
              <footer class="card-footer">
                <div class="card-footer-item">
                <button class="btn-file-input" >upload a video</button>
                <input style={{position: 'absolute'}} type="file" name="file" id="video" onChange={e => setVideo(e.target.files[0])} required />
                </div>
                <a href="#" class="card-footer-item">take a video</a>
                <a href="#" class="card-footer-item">Continue</a>
              </footer>  
            </div>
          </div>
        </div>
      </div>
          );
      }
      
const mapStateToProps = (state, ownProps) => {
  return {
            'token': state.loginReducer.token,
          
      }
    };
    
    
    export default connect(mapStateToProps)(Uploader);
