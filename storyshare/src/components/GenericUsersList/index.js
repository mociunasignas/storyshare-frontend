import React from 'react';
import './index.scss';
import GenericUserCard from "../GenericUserCard";

const GenericUsersList = ({users}) => (
    <div className='columns generic-user-list is-variable is-multiline'>
        {users.map((user, i) => <GenericUserCard user={user} key={i}/>)}
    </div>
);

export default GenericUsersList;
