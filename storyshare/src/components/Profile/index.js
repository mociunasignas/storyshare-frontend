import React, {Component} from 'react';
import {connect} from "react-redux";
import {profileAction} from "../../store/actions/profileAction";
import {profileEditAction} from "../../store/actions/profileEditAction";
import {currentTabAction} from "../../store/actions/currentTabAction";
import {followersAction} from "../../store/actions/followersAction";
import {followingAction} from "../../store/actions/followingAction";
import ProfileEdit from '../ProfileEdit'
import MyPosts from "../MyPosts";
import MyFollowers from "../MyFollowers";
import MyFollowing from "../MyFollowing";
import MyFeed from "../MyFeed";
// import './index.scss'

class Profile extends Component {

    state = {clicked: false};

    componentDidMount = async () => {
        this.props.dispatch(profileAction());
        this.props.dispatch(followersAction());
        this.props.dispatch(followingAction());
    };

    handleEditClick = () => {
        this.props.dispatch(profileEditAction());
    };

    handleTabClick = e => {
        const currentTab = e.currentTarget.firstChild.lastChild.innerText
        this.props.dispatch(currentTabAction(currentTab));
    };

    render() {

        if (this.props.profile && this.props.edit) {
            return (
                <ProfileEdit/>
            )

        } else if (this.props.profile) {
            return (
                <div className='columns'>
                    <div className='container profile'>
                        <div className='modal' id='edit-preferences-modal'>
                            <div className='modal-background'/>
                            <div className='modal-card'>
                                <header className='modal-card-head'>
                                    <p className='modal-card-title'>Edit Preferences</p>
                                    <button className='delete'/>
                                </header>
                                <section className='modal-card-body'>
                                    <label className='label'>Name</label>
                                    <p className='control'>
                                        <input className='input' placeholder='Text input' type='text'/>
                                    </p>
                                    <label className='label'>Username</label>
                                    <p className='control has-icon has-icon-right'>
                                        <input className='input' placeholder='Text input' type='text' value='pmillerk'/>
                                    </p>
                                    <label className='label'>Email</label>
                                    <p className='control has-icon has-icon-right'>
                                        <input className='input' placeholder='Email input' type='text' value='hello@'/>
                                        <i className='fa fa-warning'/>
                                        <span className='help is-danger'>This email is invalid</span>
                                    </p>
                                    <div className='control'>
                                        <div className='control-label is-pulled-left'>
                                            <label className='label'>Date of Birth</label>
                                        </div>
                                        <span>
                                            <span className='select'>
                                                <select>
                                                    <option>Month</option>
                                                    <option>With options</option>
                                                </select>
                                            </span>
                                            <span className='select'>
                                                <select>
                                                    <option>Day</option>
                                                    <option>With options</option>
                                                </select>
                                            </span>
                                            <span className='select'>
                                                <select>
                                                    <option>Year</option>
                                                    <option>With options</option>
                                                </select>
                                            </span>
                                        </span>
                                    </div>
                                    <label className='label'>Description</label>
                                    <p className='control'>
                                        <textarea className='textarea' placeholder='Describe Yourself!'/>
                                    </p>
                                    <div className='content'>
                                        <h1>Optional Information</h1>
                                    </div>
                                    <label className='label'>Phone Number</label>
                                    <p className='control has-icon has-icon-right'>
                                        <input className='input' placeholder='Text input' type='text'
                                               value='+1 *** *** 0535'/>
                                    </p>
                                    <label className='label'>Work</label>
                                    <p className='control has-icon has-icon-right'>
                                        <input className='input' placeholder='Text input' type='text'
                                               value='Greater Washington Publishing'/>
                                    </p>
                                    <label className='label'>School</label>
                                    <p className='control has-icon has-icon-right'>
                                        <input className='input' placeholder='Text input' type='text'
                                               value='George Mason University'/>
                                    </p>
                                </section>
                                <footer className='modal-card-foot'>
                                    <div className='button is-primary modal-save'>Save changes</div>
                                    <div className='button modal-cancel'>Cancel</div>
                                </footer>
                            </div>
                        </div>


                        <div className='section profile-heading' id='profile-heading'>

                            <div className='columns is-mobile is-multiline'>

                                <div className='column is-2'>
                                        <span className='header-icon user-profile-image'>
                                            <img alt=''
                                                 src={this.props.profile.profile.avatar}
                                            />
                                        </span>
                                </div>
                                <div className='column is-6-tablet is-10-mobile name'>
                                    <p>
                                            <span className='title is-bold'>
                                                {this.props.profile.first_name} {this.props.profile.last_name}
                                            </span>
                                            <div className='button is-primary is-outlined'
                                                 href='#'
                                                 id='edit-preferences'
                                                 onClick={this.handleEditClick}
                                                // style='margin: 5px 0'
                                            >
                                                Edit Profile
                                            </div>
                                        <br/>
                                    </p>
                                    <p className='tagline'>
                                        {this.props.profile.profile.about}
                                    </p>
                                    <p className='tagline'>{this.props.profile.profile.location}</p>
                                </div>
                                <div className='column is-1-tablet is-4-mobile has-text-centered'>
                                    <p className='stat-val'>{this.props.profile.fame_index}</p>
                                    <p className='stat-key'>likes</p>
                                </div>
                                <div className='column is-1-tablet is-4-mobile has-text-centered'>
                                    <p className='stat-val'>{this.props.profile.post_count}</p>
                                    <p className='stat-key'>posts</p>
                                </div>
                                <div className='column is-1-tablet is-4-mobile has-text-centered'>
                                    <p className='stat-val'>{this.props.profile.follower_count}</p>
                                    <p className='stat-key'>followers</p>
                                </div>
                                <div className='column is-1-tablet is-4-mobile has-text-centered'>
                                    <p className='stat-val'>{this.props.profile.followee_count}</p>
                                    <p className='stat-key'>following</p>
                                </div>
                            </div>
                        </div>
                        <div className='profile-options is-fullwidth'>
                            <div className='tabs is-fullwidth is-medium'>
                                <ul>
                                    <li className='link'
                                        onClick={((e) => {
                                            this.handleTabClick(e)
                                        })}
                                    >
                                        <div>
                                            <span className='icon'>
                                                <i className='fa fa-list'/>
                                            </span>
                                            <span>My Feed</span>
                                        </div>
                                    </li>
                                    <li className='link is-active'
                                        onClick={this.handleTabClick}
                                    >
                                        <div>
                                            <span className='icon'>
                                                <i className='fa fa-thumbs-up'/>
                                            </span>
                                            <span>My Posts</span>
                                        </div>
                                    </li>
                                    <li className='link'
                                        onClick={this.handleTabClick}
                                    >
                                        <div>
                                            <span className='icon'>
                                                <i className='fa fa-search'/>
                                            </span>
                                            <span>My Followers</span>
                                        </div>
                                    </li>
                                    <li className='link'
                                        onClick={this.handleTabClick}
                                    >
                                        <div>
                                            <span className='icon'>
                                                <i className='fa fa-balance-scale'/>
                                            </span>
                                            <span>Users I follow</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        {this.props.currentTab === "My Feed" ? <MyFeed/> : null}
                        {this.props.currentTab === "My Posts" ? <MyPosts/> : null}
                        {this.props.currentTab === "My Followers" ? <MyFollowers/> : null}
                        {this.props.currentTab === "Users I follow" ? <MyFollowing/> :
                            <h1>Try following someone & post more content!</h1>}
                    </div>
                </div>

            )
        } else {
            return (
                <div className='profile'>
                    <h1>My Profile loading...</h1>
                </div>)
        }
    }
}

const mapStateToProps = (state) => {
    const userdata = state.profileReducer.profile ? state.profileReducer.profile : null;
    const currentTab = state.currentTabReducer.currentTab ? state.currentTabReducer.currentTab : null;
    return {
        profile: userdata,
        edit: state.profileEditReducer.edit,
        currentTab: currentTab
    }
};

export default connect(mapStateToProps)(Profile);
