import React, {Component} from 'react';
import {connect} from 'react-redux';
import {myPostsAction} from '../../store/actions/myPostsAction';
import {profileAction} from '../../store/actions/profileAction';
import {getCategories} from '../../store/actions/categoryAction';
import {myFeedAction} from '../../store/actions/myFeedAction';
import GenericPostList from "../GenericPostList";
import PostEdit from '../PostEdit'
import './index.scss';

class MyPosts extends Component {
    componentDidMount = async () => {
        await this.props.dispatch(profileAction());
        await this.props.dispatch(getCategories());
        await this.props.dispatch(myPostsAction());
        await this.props.dispatch(myFeedAction());
    };

    render() {
        if (this.props.my_posts && this.props.postedit === true) {
            return (
                <PostEdit/>
            )
        } else if (this.props.my_posts) {
            return (
                <GenericPostList posts={this.props.my_posts}/>
            )
        } else {
            return (
                <div>hello</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const my_ID = state.profileReducer.profile ? state.profileReducer.profile.id : null;
    const my_posts = state.myPostReducer.my_posts ? state.myPostReducer.my_posts : null;
    const postedit = state.postEditReducer.postedit ? state.postEditReducer.postedit : null;

    return {
        my_ID: my_ID,
        my_posts: my_posts,
        postedit: postedit
    }
};

export default connect(mapStateToProps)(MyPosts);
