import React, {useState, useEffect} from 'react';
import {connect} from 'react-redux';
import {Link, withRouter} from 'react-router-dom';
import {currentSearchAction} from "../../../store/actions/currentSearchAction";
import {searchPostsAction} from "../../../store/actions/searchPostsAction";
import {searchUsersAction} from "../../../store/actions/searchUsersAction";
import './index.scss';

const HeaderSearchBar = (props) => {
    const [state, setState] = useState("");

    const changeSearch = (e) => {
        setState(e.target.value);
    };

    useEffect(() => {
        props.dispatch(currentSearchAction(state));
        // eslint-disable-next-line
    }, [state]);

    useEffect(() => {
        props.dispatch(searchPostsAction(props));
        props.dispatch(searchUsersAction(props));
    }, [props]);

    return (

        <div className='header-search-bar is-hidden-mobile'>
            <div className="control control-overwrite has-icons-left has-icons-right">
                <input className="input input-overwrite"
                       type="search"
                       placeholder="Search"
                       onChange={changeSearch}
                       value={state}
                />
                <span className="icon is-small is-left"><i className="fas fa-search"> </i></span>
            </div>
            <Link to='/' className='button'>Search</Link>
        </div>
    )
};

const mapStateToProps = (state) => {
    const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    return {search: search}
};

export default withRouter(connect(mapStateToProps)(HeaderSearchBar));
