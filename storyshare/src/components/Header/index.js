import React from 'react';
import {Link, withRouter} from 'react-router-dom';
import './index.scss';
import HeaderAuth from "./HeaderAuth";
import HeaderSearchBar from "./HeaderSearchBar";
import {createPostModal} from '../../store/actions/createPostAction' 
import { connect } from 'react-redux';



const Header = ({dispatch, modalStateReduxPost}) => {

    const setToggleModalPost = () => {
        let inputReduxModal
        modalStateReduxPost ? inputReduxModal = false : inputReduxModal = true
        dispatch(createPostModal(inputReduxModal));
    }

    return (
        <header className='main-header'>
            <Link to='/'>
                <img className='main-header-logo' src='https://cdn.storyshare.co/demo/client1_logo_a7250a1c38e394fa1062.png' alt="logo"/>
            </Link>

            <nav className='main-header-nav is-hidden-mobile'>
                <HomeNavItem />
                <UsersNavItem />
            </nav>

            <HeaderSearchBar />
            {/* eslint-disable-next-line */}
            <a className='button btn-new' onClick={() => setToggleModalPost()}><i className="fas fa-plus"></i></a>
            <HeaderAuth/>
        </header>
    );
};

const mapStateToProps = (state) => {
    return {
      'modalStateReduxPost': state.CreatePostModalReducer.modalPost
    }
};


export default withRouter(connect(mapStateToProps)(Header));


const HomeNavItem = () => (
    <Link to='/'>
        <i className="fas fa-home"> </i>
        <span>Feed</span>
    </Link>
);

const UsersNavItem = () => (
    <Link to='/users'>
        <i className="fas fa-users"> </i>
        <span>Users</span>
    </Link>
);
