import React, {useState, useEffect} from 'react';
import {  Link, withRouter } from 'react-router-dom';
import { connect } from "react-redux";
import './index.scss';
import {profileAction} from "../../../store/actions/profileAction";
// import HeaderNotifications from './HeaderNotifications';

const HeaderAuth = (props) => {
    const [isActive, setIsActive] = useState(false);
    const token = localStorage.getItem('token');
    let authenticated = !!token;

    const logoutHandler = (e) => { // TODO refactor to use logout action in the future
        console.log(props);
        localStorage.clear();
        props.history.push('/');
    };

    const showHideHandler = (e) => {
        e.stopPropagation();
        setIsActive(!isActive);

        // add a listener that will close the dropdown when we click anywhere on the document
        document.addEventListener('click', function _closeDropdown() {
            setIsActive(false);
            document.removeEventListener('click', _closeDropdown, true); // removes the listener
        }, true);
    };

    useEffect(() => {
        props.dispatch(profileAction());
        // eslint-disable-next-line
    }, []);

    useEffect(() => {
       // console.log(isActive);
    }, [isActive]);


    const chooseVisibleActions = () => {
        if (authenticated) {
            return (
                <HeaderProfileDropdown
                    showHideHandler={showHideHandler}
                    logoutHandler={logoutHandler}
                    isActive={isActive}
                />);
        } else {
            return (
                <>
                    <Link className='button' to='/register'>Signup</Link>
                    <Link className='button' to='/login'>Login</Link>
                </>
                )
            }
        };

    return (<div className='main-header-auth'>{ chooseVisibleActions() }</div>);
};


const mapStateToProps = (state, ownProps) => {
    const user = state.profileReducer ? state.profileReducer.profile : null;
    return {
        "user": user
    }
};

export default withRouter(connect(mapStateToProps)(HeaderAuth));


function HeaderProfileDropdown({showHideHandler, logoutHandler, isActive, user}) {
    const fullNameURL = user ? `${user.first_name}+${user.last_name}` : 'John+Doe';
    const fullName = (user && user.first_name) ? `${user.first_name} ${user.last_name}` : 'John Doe';
    return (
        <div className={"dropdown is-right" + (isActive ? ' is-active' : '')}>
            <figure className="image is-48x48" onClick={showHideHandler} onBlur={showHideHandler}>
                {/*<img src={`https://avatars.dicebear.com/v2/female/1.svg`} alt="Placeholder" />*/}
                <img src={`https://ui-avatars.com/api/?background=0D8ABC&color=fff&size=128&name=${fullNameURL}`} alt="user"/>
            </figure>

            <div className="dropdown-menu" id="dropdown-menu3" role="menu">
                <div className="dropdown-content">
                    <div className="dropdown-item">
                        <p>Hello <strong>{fullName}</strong></p>
                    </div>
                    <hr className="dropdown-divider" />
                    <Link to="/me" className="dropdown-item">
                        Profile
                    </Link>
                    <Link to="/myposts" className="dropdown-item">
                        My Posts
                    </Link>
                    <Link to="/" className="dropdown-item">
                        Something
                    </Link>
                    <Link to="/" className="dropdown-item">
                        Settings
                    </Link>
                    <hr className="dropdown-divider" />
                        <Link to="/" className="dropdown-item" onClick={logoutHandler}>Logout</Link>
                </div>
            </div>
        </div>
    )
}
