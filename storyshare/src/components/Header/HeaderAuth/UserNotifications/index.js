import React from 'react';
// import {Link, withRouter} from 'react-router-dom';
import './index.scss';


const HeaderNotifications = (props) => (
        <div className='header-notifications'>
            <i className="far fa-bell"> </i>
        </div>
);

export default HeaderNotifications;
