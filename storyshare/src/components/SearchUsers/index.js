import React, {Component} from 'react';
import {connect} from 'react-redux';
import './index.scss';
import GenericUsersList from "../GenericUsersList";

class SearchUsers extends Component {

    render() {
        if (this.props.search) {
            return (
                <div className='latest-users'>
                    <GenericUsersList users={this.props.search_users}/>
                </div>
            )
        } else {
            return (
                <div>no matching results found, sorry!</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    const search_users = state.searchUserReducer.search_users ? state.searchUserReducer.search_users : null;
    return {
        search: search,
        search_users: search_users
    }
};

export default connect(mapStateToProps)(SearchUsers);
