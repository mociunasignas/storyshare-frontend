import React from "react";

function RegisterMixinTwo({ data, registerMe, stage, setStage, setDataHandler}) {
    return (
        <div>
        {console.log(stage)}
            <form onSubmit={registerMe} className="register-form">
            <input
                className='input'
                type="text"
                value={data.code}
                placeholder="Code"
                onChange={setDataHandler}
                name="code"
            />
            <input
                className='input'
                type="email"
                value={data.email}
                placeholder="Email"
                onChange={setDataHandler}
                name="email"
            />
            <input
                className='input'
                type="text"
                value={data.first}
                placeholder="Firstname"
                onChange={setDataHandler}
                name="first_name"
            />
            <input
                className='input'
                type="text"
                value={data.last}
                placeholder="Lastname"
                onChange={setDataHandler}
                name="last_name"
            />
            <input
                className='input'
                type="password"
                value={data.password}
                placeholder="password"
                onChange={setDataHandler}
                name="password"
            />
            <input
                className='input'
                type="password"
                value={data.passwordRepeat}
                placeholder="repeat password"
                onChange={setDataHandler}
                name="password_repeat"
            />
            <button className='button' type="submit">Register</button>
            </form>
            <button className='button' onClick={() => setStage(stage-1)}>back</button>
        </div>
    );
}

export default RegisterMixinTwo;
