import React from "react";
// import './index.scss';


const RegisterMixinOne = ({data, stages, stage, setStage, setDataHandler, requestCode}) => (
    <div>
    
      <form onSubmit={requestCode} className="register-form">
        <input className='input' type="email" value={data.email} placeholder="Email" onChange={setDataHandler} name="email" />
        <button className='button' type="submit">request Validation Code</button>
      </form>
      <button className='button' onClick={() => setStage(stage+1)}>Continue</button>
    </div>
);

export default RegisterMixinOne;