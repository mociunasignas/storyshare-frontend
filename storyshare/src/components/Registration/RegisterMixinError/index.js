import React from "react";

const RegisterMixinError = ({ setStage }) => (
    <div>
       <p>Something went wrong</p>
       <button className='button' onClick={() => setStage(1)}>try again</button> 
    </div>
);


export default RegisterMixinError;
// TODO remove hardcoded stage
