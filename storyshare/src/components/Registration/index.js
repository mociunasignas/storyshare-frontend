import React, { useState } from "react";
import { connect } from "react-redux";
import axios from "axios";
import { URL } from '../../constants'
import './index.scss';
import RegisterMixinOne from "./RegisterMixinOne";
import RegisterMixinTwo from "./RegisterMixinTwo";
import RegisterMixinError from "./RegisterMixinError";
import RegisterMixinSuccess from "./RegisterMixinSuccess";


function Registration({ history }) {
  const stages = {'one': 1, 'two': 2, 'success': 3, 'error': 4}; // enum for switch
  const [stage, setStage] = useState(stages.one);
  const [data, setData] = useState({
      email: '',
      code: '',
      first_name: '',
      last_name: '',
      password: '',
      password_repeat: ''  // TODO check for matching pw before sending request
  });


  const setDataHandler = (e) => {
    const newData = {...data};
    newData[e.target.name] = e.target.value
    setData(newData);
  }

  const requestCode = async e => {
      e.preventDefault();
      try {
          const res = await axios.post(`${URL}registration/`, { "email": data.email });
          console.log("requestCodeResponse:", res);
          setStage(stages.two);
          history.push('register'); // removes the url querystring added by submitting
      } catch (e) {
        setStage(stages.error);
    }
  };

    const registerMe = async e => {
      e.preventDefault();
      try {
          const res = await axios.post(`${URL}registration/validation/`, data);
          console.log("registerResponse:", res);
          history.push('register'); // removes the url querystring added by submitting
          setStage(stages.success);
      } catch (e) {
        setStage(stages.error);
    }
  };

    // TODO maybe remove switch, this might work with mixins inside an object with the stage as key (less code)
    // TODO looks like shit. Create a HOC or use React.cloneElement to pass the data and methods to mixins
    const renderStageMixin = (stage) => {
        switch(stage) {
          case stages.one:
            return <RegisterMixinOne data={data} stages={stages} stage={stage} setStage={setStage} setDataHandler={setDataHandler} requestCode={requestCode}/>;
          case stages.two:
            return <RegisterMixinTwo data={data} stages={stages} stage={stage} setStage={setStage} setDataHandler={setDataHandler} registerMe={registerMe} />;
          case stages.success:
              return <RegisterMixinSuccess data={data}/>;
          case stages.error:
              return <RegisterMixinError setStage={setStage}/>;
          default:
              return <RegisterMixinOne data={data} stages={stages} stage={stage} setStage={setStage} setDataHandler={setDataHandler} requestCode={requestCode}/>;
        }
    }

  return (
      <div className='register-view container is-fluid"'>
        {renderStageMixin(stage)}
      </div>
  );
}

export default connect()(Registration);

