import React from "react";
import {  Link } from 'react-router-dom';


const RegisterMixinSuccess = ({ data }) => (
    <div>
       <p> Registration Successful</p>
       <p>{`Welcome to Storyshare ${data.first_name}`}</p>
       <Link className='button' to='/' >Enter Storyshare</Link>
    </div>
);


export default RegisterMixinSuccess;
// TODO remove hardcoded stage