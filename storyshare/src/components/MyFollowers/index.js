import React, {Component} from 'react';
import {connect} from 'react-redux';
import GenericUsersList from '../GenericUsersList'
// import './index.scss';

class MyFollowers extends Component {

    render() {
        if (this.props.my_followers) {
            console.log("ready to render followers")
            return (
                <GenericUsersList users={this.props.my_followers}/>
            )
        } else {
            return (
                <div>hello</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const my_followers = state.followersReducer.data ? state.followersReducer.data : null;
    return {
        my_followers: my_followers
    }
};

export default connect(mapStateToProps)(MyFollowers);
