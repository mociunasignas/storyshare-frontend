import React, {Component} from 'react';
import {connect} from 'react-redux';
import GenericUsersList from '../GenericUsersList'
// import './index.scss';

class MyFollowing extends Component {

    render() {
        if (this.props.my_following) {
            return (
                <GenericUsersList users={this.props.my_following}/>
            )
        } else {
            return (
                <div>hello</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const my_following = state.followingReducer.data ? state.followingReducer.data : null;
    return {
        my_following: my_following
    }
};

export default connect(mapStateToProps)(MyFollowing);
