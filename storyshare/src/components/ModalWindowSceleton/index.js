import React, { useState } from 'react';
import ModalWindowBody from '../ModalWindowBody';

const ModalWindowSceleton = ({ children, setToggleModal, modalState, title }) => {
  
    
  
  if(!modalState) {
  return null;
}
    


    

    return(
      
      <div className="modal is-active">
      <div className="modal-background" onClick={() => setToggleModal(false)} />
      <div className="modal-card">
          <header className="modal-card-head">
              <p className="modal-card-title">Share Me</p>
              <button className="delete" onClick={() => setToggleModal(false)} />
          </header>
          <ModalWindowBody
              setToggleModal={() => setToggleModal()}
              modalState={modalState}
          >
              </ModalWindowBody>
             
          <footer className="modal-card-foot">
              <a className="button" onClick={() => setToggleModal(false)}>Cancel</a>
          </footer>
      </div>
  </div>

          
    );
  }

export default ModalWindowSceleton