import React, {Component} from 'react';
import {connect} from "react-redux";
import './index.scss'
import {profileNoEditAction} from "../../store/actions/profileEditAction";
import {profileUpdateAction} from "../../store/actions/profileUpdateAction";
import {profileAction} from "../../store/actions/profileAction";

class ProfileEdit extends Component {

    state = {
        "username": this.props.profile.username,
        "first_name": this.props.profile.first_name,
        "last_name": this.props.profile.last_name,
        "email": this.props.profile.email,
        "location": this.props.profile.profile.location,
        "about": this.props.profile.profile.about,
        "phone": this.props.profile.profile.phone,
    };

    handleUserChange = e => {
        this.setState({
            username: e.currentTarget.value // here goes the value from the input
        })
    };
    handleEmailChange = e => {
        this.setState({
            email: e.currentTarget.value // here goes the value from the input
        })
    };
    handleFirstChange = e => {
        this.setState({
            first_name: e.currentTarget.value // here goes the value from the input
        })
    };
    handleLastChange = e => {
        this.setState({
            last_name: e.currentTarget.value // here goes the value from the input
        })
    };
    handleAboutChange = e => {
        this.setState({
            about: e.currentTarget.value // here goes the value from the input
        })
    };
    handlePhoneChange = e => {
        this.setState({
            phone: e.currentTarget.value // here goes the value from the input
        })
    };
    handleLocationChange = e => {
        this.setState({
            location: e.currentTarget.value // here goes the value from the input
        })
    };
    handleClick = () => {
        this.props.dispatch(profileNoEditAction());
    };

    handleSubmit = async () => {

       await this.props.dispatch(profileUpdateAction(this.state));
        this.props.dispatch(profileAction());
        this.props.dispatch(profileNoEditAction());
    };

    render() {
        if (this.props.profile) {
            return (
                <div className='profile'>
                    <h1>Edit My Profile</h1>

                    {/*Profile Edit View*/}
                    <div className='is-active' id='edit-preferences-modal'>
                        <div className='modal-background'/>
                        <div className='modal-card'>

                            <header className='modal-card-head'>
                                <p className='modal-card-title'>Edit Profile</p>
                                <button className='delete'
                                        onClick={this.handleClick}
                                />
                            </header>

                            <section className='modal-card-body'>

                                <span className='edit-name'>
                                    <div>
                                        <label className='label'>Username</label>
                                        <p className='control'>
                                            <input className='input'
                                                   placeholder='Text input'
                                                   type='text'
                                                   value={this.state.username}
                                                   onChange={this.handleUserChange}

                                            />
                                        </p>
                                    </div>

                                    <div>
                                        <label className='label'>Email</label>
                                        <p className='control has-icon has-icon-right'>
                                            <input className='input'
                                                   placeholder='Email input'
                                                   type='text'
                                                   value={this.state.email}
                                                   onChange={this.handleEmailChange}
                                            />
                                        </p>
                                    </div>
                                </span>

                                <span className='edit-name'>
                                    <div>
                                        <label className='label'>First Name</label>
                                        <p className='control'>
                                            <input className='input'
                                                   placeholder='Text input'
                                                   type='text'
                                                   value={this.state.first_name}
                                                   onChange={this.handleFirstChange}
                                            />
                                        </p>
                                    </div>

                                    <div>
                                        <label className='label'>Last Name</label>
                                        <p className='control'>
                                            <input className='input'
                                                   placeholder='Text input'
                                                   type='text'
                                                   value={this.state.last_name}
                                                   onChange={this.handleLastChange}
                                            />
                                        </p>
                                    </div>
                                </span>

                                <span className='edit-name'>
                                    <div>
                                        <label className='label'>Location</label>
                                        <p className='control'>
                                            <input className='input'
                                                   placeholder='Text input'
                                                   type='text'
                                                   value={this.state.location}
                                                   onChange={this.handleLocationChange}

                                            />
                                        </p>
                                    </div>

                                    <div>
                                        <label className='label'>Phone</label>
                                        <p className='control has-icon has-icon-right'>
                                        <input className='input'
                                               placeholder='Text input'
                                               type='text'
                                               value={this.state.phone}
                                               onChange={this.handlePhoneChange}

                                        />
                                      </p>
                                    </div>
                                </span>

                                <label className='label'>About</label>
                                <p className='control'>
                                    <textarea className='textarea'
                                              placeholder='Describe Yourself!'
                                              value={this.state.about}
                                              onChange={this.handleAboutChange}
                                    >x</textarea>
                                </p>

                            </section>
                            <footer className='modal-card-foot'>
                                <div className='button purple modal-save'
                                   onClick={this.handleSubmit}
                                >Save changes</div>
                                <div className='button modal-cancel'
                                   onClick={this.handleClick}
                                >Cancel</div>
                            </footer>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div>content loading...</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const userdata = state.profileReducer.profile ? state.profileReducer.profile : null;
    return {profile: userdata}
};

export default connect(mapStateToProps)(ProfileEdit);
