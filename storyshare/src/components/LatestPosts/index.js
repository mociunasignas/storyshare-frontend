import React, { useEffect } from 'react';
import { connect } from 'react-redux';
import './index.scss';
import GenericPostList from "../GenericPostList";
import {objToArrOrdered} from '../../helpers'
import { savePostsAction } from '../../store/actions/postActions';
import Paginator from "../Paginator";


// This view displays latest posts (10 are fetched at a time, hardcoded in the backend pagination class)
const LatestPosts = ({posts, dispatch, current, isLoaded}) =>  {

    useEffect(() => {
    if (!isLoaded) {
        dispatch(savePostsAction());
        // eslint-disable-next-line
    }}, [current]);


    return (
        <div className='latest-posts'>
            <GenericPostList posts={ posts }/>
            <Paginator/>
        </div>
    )
};


const mapStateToProps = (state, ownProps) => {
    return {
        'posts': objToArrOrdered(state.postReducer, state.paginatorReducer.latestPosts.byId),
        'current': state.paginatorReducer.latestPosts.current,
        'isLoaded': false // TODO remove hardcoding. This is supposed to prevent refetches of already available posts
    }
};

export default connect(mapStateToProps)(LatestPosts);
