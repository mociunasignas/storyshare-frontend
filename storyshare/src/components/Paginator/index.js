import React from "react";
 import { connect } from "react-redux";
import './index.scss';
import { setPostsCurrentPageAction } from '../../store/actions/paginatorActions'


// TODO make this into a generic component and pass actions that should happen via props
const Paginator = ({dispatch, next, prev, current, isLoaded}) => {
    // useEffect(() => {
    //     if (!isLoaded) {
    //         dispatch(savePostsAction());
    //     }}, [current]);

    const onNext = () => {
        console.log("next");
        if (next) {
            dispatch(setPostsCurrentPageAction({ current: current+1 }));
        }
    };


    // DONT DELETE
    // const onPrev = () => {  // no previous required because
    //     console.log("prev");
    //     if (prev) {
    //         dispatch(setPostsCurrentPageAction({ current: current-1 }));
    //     }
    // };


    return (
        <div>
            {/*<button className="button" onClick={onPrev}>Prev</button>*/}
            <button className="button" onClick={onNext}>More</button>
        </div>
    );
};


const mapStateToProps = (state, ownProps) => {
    return {
        'next': state.paginatorReducer.latestPosts.next,
        'prev': state.paginatorReducer.latestPosts.prev,
        'total': state.paginatorReducer.latestPosts.total,
        'current': state.paginatorReducer.latestPosts.current,
        'isLoaded': state.paginatorReducer.latestPosts.isLoaded
    }
};


export default connect(mapStateToProps)(Paginator);
