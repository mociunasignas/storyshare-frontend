import React from 'react';
import CreatePost from './CreateRespond'

const ModalWindowBodyPost = ({ children, setToggleModalPost, modalStateReduxPost, title }) => {

    
    if(!modalStateReduxPost) {
      return null;
    }
    
    return(
          <section className="modal-card-body">
            <div className="content">
            
           <CreatePost />
            </div>
          </section>
    );
  }

export default ModalWindowBodyPost