import React, { useState } from 'react';
import Uploader from './Uploader';
import CreatePostForm from './CreateRespondForm'
import axios from 'axios';
import uuid from "uuid";
import { URL } from '../../../../../constants';
import { connect } from 'react-redux';


const CreatePost = ({ token }) => {


    const [videoUUID, setVideoUUID] = useState(null)
    const [copyButton, setCopyButton] = useState('btn-file-input');
    const [buttonText, setButtonText] = useState('upload a video')

    const handleSubmitInput = (e) => {
        e.preventDefault();
        let form_data = new FormData();
        form_data.append('video_uuid', videoUUID);
        let url = `${URL}respond/`;
        axios.post(url, form_data, {
            headers: {
                Authorization: `Bearer ${token}`,
            }
        }).then(res => {
        console.log(res.data);
        }).catch(err => console.log('err', err))
    
      };
    

    const handleSubmitVideo = (video) => {
        const video_uuid = uuid.v4()
        let form_data = new FormData();
        form_data.append('file', video, `${video_uuid}`);
        form_data.append('video_uuid', `${video_uuid}`);
        form_data.append('thumbnail', video, `${video_uuid}`);
        let url = `${URL}upload/`;
        axios.post(url, form_data, {
            headers: {
                Authorization: `Bearer ${token}`,
                'content-type': 'multipart/form-data'
            }
        }).then(
            console.log('worked video')
            ).catch(err => console.log(err))
            setVideoUUID(`${video_uuid}`);
            
    }




    const makeChange = (videoFile) => {
        const UUID = handleSubmitVideo(videoFile)
        setCopyButton('btn-file-input-ok')
        setButtonText('OK!')
    }

    

    return (
        <div className='whole-wrapper-create-post'>
            <Uploader style={{ maxWidth: '650px', border: 'solid red' }} />
            <CreatePostForm
                tags={tags} 
                setTags={setTags} 
                tagsInput={tagsInput} 
                setTagsInput={setTagsInput} 
                optionID={optionID}
                setOptionID={setOptionID}
                title={title}
                setTitle={setTitle}
                />

            <footer className="card-footer">
                <div className="card-footer-item">
                    <button className={copyButton} >{buttonText}</button>
                    <input style={{ position: 'absolute' }} type="file" name="video" id="video" onChange={e => makeChange(e.target.files[0])} required />
                </div>
                <div className="card-footer-item">
                    <button className="btn-file-input" >record a video</button>
                    <input style={{ position: 'absolute' }} type="file" name="file" onChange={e => console.log('record')} required />
                </div>
                <div className="card-footer-item">
                    <button className="btn-file-input" onClick={(e) => handleSubmitInput(e)} >continue</button>
                </div>
            </footer>
        </div>
    );
}


const mapStateToProps = (state, ownProps) => {
    // console.log("STATE:", state)
return {
    'token': state.loginReducer.token,
  
}
};

export default connect(mapStateToProps)(CreatePost)

