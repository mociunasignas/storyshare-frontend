import React from 'react';
import ModalWindowBodyPost from './ModalWindowBodyRespond';

const ModalWindowSceletonPost = ({ children, setToggleModalPost, modalStateReduxPost, title }) => {

  if (!modalStateReduxPost) {
    return null;
  }

  return (

    <div className="modal is-active">
      <div className="modal-background" onClick={() => setToggleModalPost(false)} />
      <div className="modal-card">
        {/* <header className="modal-card-head">
              <p className="modal-card-title">Share Me</p>
              <button className="delete" onClick={() => setToggleModal(false)} />
          </header> */}
        <ModalWindowBodyPost
          setToggleModalPost={() => setToggleModalPost()}
          modalStateReduxPost={modalStateReduxPost}
        >
        </ModalWindowBodyPost>

        <footer className="modal-card-foot">
          {/* eslint-disable-next-line */}
          <a className="button" onClick={() => setToggleModalPost(false)}>Cancel</a>
        </footer>
      </div>
    </div>
  );
}

export default ModalWindowSceletonPost