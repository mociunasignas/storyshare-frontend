import React from 'react';
import ModalWindowSceletonPost from './ModalWindowSceletonRespond'
import { connect } from 'react-redux';
import {createPostModal} from '../../store/actions/createPostAction' 

const CreatePost = ({modalStateReduxPost, dispatch,}) => {

    const setToggleModalPost = (input) => {
            dispatch(createPostModal(input));
    }

    return (
        <div className='createPostWrapper'>
            <section className="section">
                <div className="container">
                    <div className="has-text-centered content">
                        <hr />         
                    </div>

                <ModalWindowSceletonPost modalStateReduxPost={modalStateReduxPost} setToggleModalPost={setToggleModalPost}  />


                </div>
            </section>

        </div>
    )
}

const mapStateToProps = (state) => {
    return {
      'modalStateReduxPost': state.CreatePostModalReducer.modalPost
    }
  };
  
  
  export default connect(mapStateToProps)(CreatePost);
