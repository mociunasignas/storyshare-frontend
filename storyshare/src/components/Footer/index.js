import React from 'react';
import './index.scss';

 // TODO change path of icons for deployment
const Footer = (props) => {
    return (
        <footer className="main-footer footer">
            <div className="center">
                <nav className="main-footer-nav">
                    <a href="#a">Company</a>
                    <a href="#a">Products</a>
                    <a href="#a">Contact Us</a>
                    <a href="#a">Terms</a>
                    <a href="#a">DMCA</a>
                </nav>
                <p className="copyright">&copy; 2019 StoryShare</p>
            </div>

        </footer>
    );
};

export default Footer;