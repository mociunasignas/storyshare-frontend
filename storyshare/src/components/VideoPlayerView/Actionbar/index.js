import React from "react";
import { connect } from "react-redux";
import './index.scss';
import LikeButton from "./LikeButton";
import  Share  from '../../Share'
import { shareModal } from '../../../store/actions/shareAction'


// TODO add functionality
const Actionbar = ({ dispatch, shareModalStateRedux, match }) => {


    const setToggleModalShare = () => {
        let inputReduxModal
        shareModalStateRedux ? inputReduxModal = false : inputReduxModal = true
        dispatch(shareModal(inputReduxModal));
    }

    

        return (
            <div className="actionbar">
                <LikeButton postId={match.params.post_id} />
                <div className="actionbar-action">
                    <i className="fas fa-reply"> </i>
                    <div>Reply</div>
                </div>
                <Share postURL={window.location.href} shareModalStateRedux={shareModalStateRedux} setToggleModalShare={() => setToggleModalShare()} />
                <div className="actionbar-action">
                    <i className="fas fa-flag"> </i>
                    <div>Report</div>
                </div>
            </div>
        );
    }




    const mapStateToProps = (state) => {
        return {
          'shareModalStateRedux': state.ShareModalReducer.modalShare
        }
      };
      
      
      export default connect(mapStateToProps)(Actionbar);
    
