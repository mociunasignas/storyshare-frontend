import React, {useEffect} from 'react';
import { connect } from 'react-redux';
import './index.scss';
import { likeAPostAction } from '../../../../store/actions/likeAction';

const LikeButton = ({ postId, dispatch }) => {

    useEffect(() => {
        // todo make a fetch to check if post is liked, cache it in redux
    }, []);
    const likeHandler = () => dispatch(likeAPostAction(postId));
    let isLiked = false; // TODO remove hardcoding
    return (
        <div className="btn-like actionbar-action"  onClick={ likeHandler }>
              <i className={`fas fa-thumbs-up ${isLiked ? 'active' : ''}`}> </i>
              <div>Like</div>
        </div>
    )
};

// const mapStateToProps = (state, ownProps) => {  // needed later on
//     return {
//         'isLiked': state.likedPostsReducer[ownProps.postId],  // might be used
//         'token': state.loginReducer.token
//     }
// };


export default connect()(LikeButton);