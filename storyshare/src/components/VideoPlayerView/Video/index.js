import React, { useState } from "react";
import { connect } from "react-redux";
import './index.scss';
// import {CircularProgressbar} from "react-circular-progressbar";
// import { Player } from 'video-react';


const Video = ({videoEndedHandler, videoRef, url}) => {
    const [state, setState] = useState({
        playing: false,
        hideClass: "",
        progress: 0
    });

    const playOrPauseVideo = () => {
        console.log("click");
        const video = videoRef.current;
        video.paused ? video.play() : video.pause();
    };

    const setProgressTest = () => {
        const currentProgress = videoRef.current.currentTime / videoRef.current.duration;
        setState({...state, ...{progress: currentProgress}});
    };

    return (
        <div className="video-player-wrapper">
            <div className={"overlay " + state.hideClass} onClick={playOrPauseVideo}>
                <div className="play-btn" > </div>
            </div>

            <video
                onTimeUpdate={setProgressTest}
                className="video-player"
                ref={videoRef}
                key={url}
                onEnded={videoEndedHandler}
                onPlay={() => setState({...state, ...{playing: true, hideClass: "hide"}}) }
                onPause={() => setState({...state, ...{playing: false, hideClass: ""}}) }
            >
            <source src={url + '#t=0'} />
            </video>
            {/*<CircularProgressbar value={state.progress} maxValue={1} />;*/}
        </div>
    )
};

export default connect()(Video);

