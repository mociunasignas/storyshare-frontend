import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import './index.scss';
import Video from './Video';
import { videoURL } from '../../constants'
import VideoReplyList from "./VideoReplyList";
import {objToArrOrdered} from '../../helpers'
import Actionbar from "./Actionbar";
import {saveRepliesAction} from "../../store/actions/saveRepliesAction";
import {getActualPostAction} from '../../store/actions/getActualPostAction'
// import { CircularProgressbar } from 'react-circular-progressbar';
// import 'react-circular-progressbar/dist/styles.css';


const VideoPlayerView = ({post, replies, dispatch, match, fallbackPost, shareModalStateReduxShare}) => {
    const videoRef = React.useRef(null); // reference to the <video> element
    const [current, setCurrent] = useState(0);
    const currentPost = post || fallbackPost;

    useEffect(() => {
        dispatch(getActualPostAction(match.params.post_id))
    }, [dispatch, match.params.post_id]);
    

    const postVideoUrl = videoURL + currentPost.video;
    const videoUrls = [postVideoUrl, ...replies.map((reply) => (videoURL + reply.video))];
    
    useEffect(() => {
        console.log("Current", current)
        
    }, [current]);
    
    
    useEffect(() => {
        if (currentPost) {
            dispatch(saveRepliesAction(currentPost.id));
        }
    }, [dispatch, currentPost]);
    
    const videoEndedHandler = () => {
        console.log("video ended");
        const endReached = current >= replies.length;
        const newCurrent = endReached ? 0 : current+1;
        setCurrent(newCurrent);
    };

    useEffect(() => {
        // play replies automatically
        if (current !== 0) {
            videoRef.current.oncanplay = () => {
                videoRef.current.play();
            }
        }
    }, [current]);

    
    return (
      <div className="player-view-wrapper media-content">
          <div className="player-top">
              <div className="video-wrapper">
                  <div className={"button btn-back" + (current===0 ? " hide" : "")} onClick={() => setCurrent(0) }>
                      return
                  </div>
                  <Video videoEndedHandler={videoEndedHandler} url={videoUrls[current]} videoRef={videoRef}/>
              </div>
              <Actionbar match={match} />
          </div>
          <div className="player-bottom">
              <VideoReplyList replies={replies} current={current} setCurrent={setCurrent} />
              {/* {console.log("REPLIES OUTSIDE REPLY LIST", replies) } */}
          </div>
      </div>
    )
};

const mapStateToProps = (state, ownProps) => {
    // eslint-disable-next-line
    const postReplyIds = state.postReplyIdReducer;
    const postId = ownProps.match.params.post_id;
    const getReplies = (postId) => {
        if (state.postReplyIdReducer[postId]) {
            const postReplyIds = state.postReplyIdReducer[postId];
            return objToArrOrdered(state.replyReducer, postReplyIds);
        } else {
            return [];
        }
    };
    // console.log(postId)
    return {
        'post': state.postReducer[postId],
        'fallbackPost' : state.ActualPostReducer,
        'replies': getReplies(postId),
        'shareModalStateReduxShare': state.ShareModalReducer.modalShare
    }
};

export default connect(mapStateToProps)(VideoPlayerView);
