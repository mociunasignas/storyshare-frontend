import React, { useState, useEffect } from "react";
import { connect } from "react-redux";
import './index.scss';
import LikeButton from "./LikeButton";
import { withRouter } from 'react-router-dom';
import {Share} from '../../Share'
import {shareModal} from '../../../store/actions/shareAction'


// TODO add functionality
const VideoPostDetail = (props, {dispatch, ShareModalReducer}) => {

    const setToggleModal = () => {
        let inputReduxModal
        ShareModalReducer ? inputReduxModal = false : inputReduxModal = true
        dispatch(shareModal(inputReduxModal));
    }

    return (
        <div className="actionbar">
          <LikeButton postId={props.match.params.post_id}/>
          <div className="actionbar-action">
              <i className="fas fa-reply"> </i>
              <div>Reply</div>
          </div>
          <div className="actionbar-action" onClick={() => setToggleModal()}>
              <i className="fas fa-share"> </i>
              <div>Share</div>
          </div>
          <div className="actionbar-action">
              <i className="fas fa-bookmark"> </i>
              <div>Saved</div>
          </div>
          <div className="actionbar-action">
              <i className="fas fa-flag"> </i>
              <div>Report</div>
          </div>
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
      'modalStatShareModalReducereRedux': state.CreatePostModalReducer.modal
    }
};


export default withRouter(connect(mapStateToProps)(VideoPostDetail));

