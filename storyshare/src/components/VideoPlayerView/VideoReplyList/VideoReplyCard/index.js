import React from 'react';
import './index.scss';
// import {CircularProgressbar} from "react-circular-progressbar";

// TODO add custom video poster
const VideoReplyCard = ({reply, active, setCurrent, i}) => {
    const activeClass = active ? " active" : "";
    return (
        <div className={'video-reply-card' + activeClass} onClick={(e) => setCurrent(i+1)}>
            click to play reply {reply.id}
        </div>
    );
}

export default VideoReplyCard;
