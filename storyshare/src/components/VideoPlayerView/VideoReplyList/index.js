import React from "react";
import { connect } from "react-redux";
import VideoReplyCard from './VideoReplyCard'

const VideoReplyList = ({replies, current, setCurrent }) => {
  return (
      <div>
          {replies && replies.map((reply, i) => (
              <VideoReplyCard
                  current={current}
                  setCurrent={setCurrent}
                  reply={reply}
                  active={i === current-1}
                  key={i}
                  i={i}
              />)
          )}
      </div>
  )
};

export default connect()(VideoReplyList);

