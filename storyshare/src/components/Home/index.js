import React from "react";
import {connect} from "react-redux";
import LatestPosts from "../LatestPosts";
import SearchPosts from "../SearchPosts";
import './index.scss';

const Home = (props) => {
    if (props.search) {
        return (
            <SearchPosts/>
        )
    } else {

        return (
            <LatestPosts/>
        );
    }
};

const mapStateToProps = (state) => {
    const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    const search_posts = state.searchPostReducer.search_posts ? state.searchPostReducer.search_posts : null;

    return {
        search: search,
        search_posts: search_posts
    }
};

export default connect(mapStateToProps)(Home)
