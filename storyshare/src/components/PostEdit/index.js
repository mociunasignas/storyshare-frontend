import React, {Component} from 'react';
import {connect} from "react-redux";
import {postNoEditAction} from "../../store/actions/postEditAction";
import {postUpdateAction} from "../../store/actions/postUpdateAction";
import {myPostsAction} from "../../store/actions/myPostsAction";
import './index.scss'

class PostEdit extends Component {

    state = {
        "category": this.props.my_posts.category,
        "title": this.props.my_posts.title,
        "status": this.props.my_posts.status,
    };

    handleCategoryChange = e => {
        this.setState({
            category: e.currentTarget.value // here goes the value from the input
        })
    };
    handleTitleChange = e => {
        this.setState({
            title: e.currentTarget.value // here goes the value from the input
        })
    };
    handleStatusChange = e => {
        this.setState({
            status: e.currentTarget.value // here goes the value from the input
        })
    };

    handleClick = () => {
        this.props.dispatch(postNoEditAction());
    };

    handleSubmit = async () => {

        await this.props.dispatch(postUpdateAction(this.props, this.state));
        this.props.dispatch(myPostsAction());
        this.props.dispatch(postNoEditAction());
    };

    render() {
        if (this.props.my_posts && this.props.categories) {
            return (
                <div className='profile'>
                    <h1>Edit My Post</h1>

                    {/*Profile Edit View*/}
                    <div className='is-active' id='edit-preferences-modal'>
                        <div className='modal-background'/>
                        <div className='modal-card'>

                            <header className='modal-card-head'>
                                <p className='modal-card-title'>Edit Post</p>
                                <button className='delete'
                                        onClick={this.handleClick}
                                />
                            </header>

                            <section className='modal-card-body'>

                                <div>
                                    <label className='label'>Category</label>
                                    <span className='select'>
                                        <select
                                            onChange={this.handleCategoryChange}>
                                            {this.props.categories.map((cat, i) =>
                                                <option key={i}
                                                        onChange={this.handleCategoryChange}
                                                >{cat.category_name}</option>
                                            )}
                                        </select>
                                    </span>
                                </div>

                                <div>
                                    <label className='label'>Title</label>
                                    <p className='control'>
                                        <input className='input'
                                               placeholder='Text input'
                                               type='text'
                                               value={this.state.title}
                                               onChange={this.handleTitleChange}
                                        />
                                    </p>
                                </div>

                                <div>
                                    <label className='label'>Status</label>
                                    <span className='select'>
                                        <select
                                            onChange={this.handleStatusChange}>
                                                <option>DRAFT</option>
                                                <option>PUBLISHED</option>
                                            )}
                                        </select>
                                    </span>
                                    {/*<p className='control'>*/}
                                    {/*    <input className='input'*/}
                                    {/*           placeholder='Text input'*/}
                                    {/*           type='text'*/}
                                    {/*           value={this.state.status}*/}
                                    {/*           onChange={this.handleStatusChange}*/}
                                    {/*    />*/}
                                    {/*</p>*/}
                                </div>

                            </section>

                            <footer className='modal-card-foot'>
                                <div className='button purple modal-save'
                                   onClick={this.handleSubmit}
                                >Save changes</div>
                                <div className='button modal-cancel'
                                   onClick={this.handleClick}
                                >Cancel</div>
                            </footer>
                        </div>
                    </div>
                </div>
            );
        } else {
            return (
                <div>Post loading...</div>
            )
        }
    }
}

const mapStateToProps = (state) => {
    const my_posts = state.myPostReducer.my_posts ? state.myPostReducer.my_posts[0] : null;
    const postedit = state.postEditReducer.postedit ? state.postEditReducer.postedit : null;
    const categories = state.categoryReducer.categories ? state.categoryReducer.categories : null;

    return {
        my_posts: my_posts,
        postedit: postedit,
        categories: categories
    }
};

export default connect(mapStateToProps)(PostEdit);
