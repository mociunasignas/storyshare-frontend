import React, { useState } from 'react';
import ShareURL from './ShareURL'
import ShareEmbeded from './ShareEmbeded'

const Modal = ({ shareModalStateReduxShare, postURL }) => {
  
  
  const [CARD, setCARD] = useState(<ShareURL />);
  const [bottonStyleUrl, setBottonStyleUrl] = useState('button is-text')
  const [bottonStyleEmbed, setBottonStyleEmbed] = useState('button is-white')
    console.log('shareModalStateReduxShare2', shareModalStateReduxShare)
    if(!shareModalStateReduxShare) {
      return null;
    }

    const switcher = (e) => {
      switch (e) {
          case 'url':
              setCARD(<ShareURL postURL={postURL} />)
              setBottonStyleUrl('button is-white')
              break;
          case 'embed':
              setCARD(<ShareEmbeded postURL={postURL} />)
              setBottonStyleEmbed('button is-text')
              break;

          default:
              console.log(e)
              break;
      }
  }


    
    
    return(
          <section className="modal-card-body">
            <div className="content">
            {CARD}
            <button
                  className={bottonStyleUrl}
                  onClick={(e) => switcher(e.target.value)}
                  value='url'>
                  URL
              </button>

              <button
                  className={bottonStyleEmbed}
                  onClick={(e) => switcher(e.target.value)}
                  value='embed'>
                  EMBED
              </button>
          
            </div>
          </section>
    );
  }

export default Modal