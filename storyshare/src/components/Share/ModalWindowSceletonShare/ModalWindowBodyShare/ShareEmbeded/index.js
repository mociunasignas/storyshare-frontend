import React, { useState } from 'react';




const ShareEmbeded = () => {


    const [copyButton, setCopyButton] = useState('button');
    const [copyInfo, setCopyInfo] = useState('copy me');

    const copy = require('copy-text-to-clipboard');

    const embedText = (`<iframe src=${window.location.href} allow="microphone; camera" style="width:100%;max-width:600px;height:350px" frameborder="0" scrolling="no"></iframe>`)

    const copied = () => {
        copy(embedText)
        setCopyButton('button is-success')
        setCopyInfo('copied')
    }

    return (
        <div>
            <div className="box">
                {embedText}
            </div>
            <button className={copyButton} onClick={() => copied()}>{copyInfo}</button>

        </div>
    )
}


export default ShareEmbeded