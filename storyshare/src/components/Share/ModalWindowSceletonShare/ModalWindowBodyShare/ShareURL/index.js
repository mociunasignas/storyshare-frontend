import React, { useState } from 'react';
  import {
    FacebookIcon,
    TwitterIcon,
    TelegramIcon,
    WhatsappIcon,
} from 'react-share';

const ShareURL = () => {


    const [copyButton, setCopyButton] = useState('button');
    const [copyInfo, setCopyInfo] = useState('copy me');

    const copy = require('copy-text-to-clipboard');

    const copied = () => {
        copy(`${window.location.href}`)
        setCopyButton('success')
        setCopyInfo('copied')
    }

    const ShareIcons = {
        padding: '0.4rem'
      };
    const ShareIconsBox = {
        marginTop: '1rem',
        display: 'flex',
        flexdirection: 'column'
    }  
    
    
    return (
        <div>
            <div className="box">
                {window.location.href}
                </div>
            
            <button className={copyButton} onClick={() => copied()}>{copyInfo}</button>
            <div style={ShareIconsBox}>
            <div style={ShareIcons}>
                <FacebookIcon size={32} round={false} />
                </div>
            <div style={ShareIcons}>
                <TwitterIcon size={32} round={false} />
                </div>
            <div style={ShareIcons}>
                <WhatsappIcon size={32} round={false} />
                </div>
            <div style={ShareIcons}>
                <TelegramIcon size={32} round={false} />
                </div>
            </div>
            
        </div>
    )
}


export default ShareURL


