import React from 'react';
import ModalWindowBodyShare from './ModalWindowBodyShare';

const ModalWindowSceleton = ({ shareModalStateReduxShare, setToggleModalShare }) => {
  
  console.log('shareModalStateReduxShare1', shareModalStateReduxShare)
  if(!shareModalStateReduxShare) {
  return null;
}
    return(
      
      <div className="modal is-active">
      <div className="modal-background" onClick={() => setToggleModalShare(false)} />
      <div className="modal-card">
          <header className="modal-card-head">
              <p className="modal-card-title">Share Me</p>
              <button className="delete" onClick={() => setToggleModalShare(false)} />
          </header>
          <ModalWindowBodyShare
              setToggleModalShare={() => setToggleModalShare()}
              shareModalStateReduxShare={shareModalStateReduxShare}
          >
              </ModalWindowBodyShare>
             
          <footer className="modal-card-foot">
            {/* eslint-disable-next-line */}
              <a className="button" onClick={() => setToggleModalShare(false)}>Cancel</a>
          </footer>
      </div>
  </div>

          
    );
  }

export default ModalWindowSceleton