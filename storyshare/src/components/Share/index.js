import React from 'react';
import ModalWindowSceletonShare from './ModalWindowSceletonShare'
import { shareModal } from '../../store/actions/shareAction'
import { connect } from 'react-redux';


const Share = ({ shareModalStateReduxShare, setToggleModalShare, dispatch, postURL }) => {
  
    // const setToggleModalShare = (input) => {
    //     dispatch(shareModal(input));
    // }



    return (
        <div className="btn-like actionbar-action"  onClick={() => setToggleModalShare()}>
            <i className="fas fa-share" ></i>
            <div>Share</div>
            <ModalWindowSceletonShare shareModalStateReduxShare={shareModalStateReduxShare} setToggleModalShare={setToggleModalShare} postURL={postURL} />
        </div>
    )
}


const mapStateToProps = (state) => {
    return {
        'shareModalStateReduxShare': state.ShareModalReducer.modalShare
    }
};


export default connect(mapStateToProps)(Share);
