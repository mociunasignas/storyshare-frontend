import React from 'react';
import './index.scss';
import GenericPostCard from "../GenericPostCard";

const GenericPostList = ({posts}) => (
    <div className="columns is-multiline is-variable generic-post-list">
        {posts.map((post, i) => <GenericPostCard post={post} key={i}/>)}
    </div>
);

export default GenericPostList;
