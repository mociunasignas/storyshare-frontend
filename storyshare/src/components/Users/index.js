import React from "react";
import {connect} from "react-redux";
import LatestUsers from "../LatestUsers";
import SearchUsers from "../SearchUsers";
import './index.scss';

const Users = (props) => {
    if (props.search) {
        return (
            <SearchUsers/>
        )
    } else {
        return (
            <LatestUsers/>
        );
    }
};

const mapStateToProps = (state) => {
    const search = state.currentSearchReducer.search ? state.currentSearchReducer.search : null;
    const search_users = state.searchUserReducer.search_users ? state.searchUserReducer.search_users : null;

    return {
        search: search,
        search_users: search_users
    }
};

export default connect(mapStateToProps)(Users)
