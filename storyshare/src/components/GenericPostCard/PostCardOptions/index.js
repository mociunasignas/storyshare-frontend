import React, {useState} from 'react';
import {  Link, withRouter } from 'react-router-dom';
import './index.scss';
import {connect} from "react-redux";

const PostCardOptions = ({post, user}) => {
    const [isActive, setIsActive] = useState(false);
    const isPostOwner = user ? post.user.id === user.id : false;

    const showHideHandler = (e) => {
        setIsActive(!isActive);

        // add a listener that will close the dropdown when we click anywhere on the document
        document.addEventListener('click', function _closeDropdown() {
            setIsActive(false);
            document.removeEventListener('click', _closeDropdown, true); // removes the listener
        }, true);
    };


    return (
        <div className='post-card-options'>
            <PostCardDropdown
                post={post}
                showHideHandler={showHideHandler}
                isActive={isActive}
                isPostOwner={isPostOwner}
            />
        </div>
    );
};


const mapStateToProps = (state, ownProps) => {
    if (state.profileReducer) {
        return {
            'user': state.profileReducer.profile,
        }
    } else {
        return {
            // empty on purpose
        }
    }
};

export default withRouter(connect(mapStateToProps)(PostCardOptions));


function PostCardDropdown({post, showHideHandler, isActive, isPostOwner}) {
    return (
        <div className={"dropdown is-right" + (isActive ? ' is-active' : '')} onClick={showHideHandler}>
            <i className="fas fa-ellipsis-v"> </i>

            <div className="dropdown-menu" id="dropdown-menu3" role="menu">
                <div className="dropdown-content">
                    <ShowPostOwnerOnly to={`/edit/${post.id}`} isPostOwner={isPostOwner}>
                        <Link to={`/edit/${post.id}`} className="dropdown-item">Edit Post</Link>
                    </ShowPostOwnerOnly>
                    <Link to="/" className="dropdown-item">
                        Delete Post (not hooked up)
                    </Link>
                    <Link to="/" className="dropdown-item">
                        Something
                    </Link>
                </div>
            </div>
        </div>
    )
}


// the owner must come from outside.
// if not owner it will not display children
function ShowPostOwnerOnly({isPostOwner, to, children}) {
    if (isPostOwner) {
        return <>{children}</>
    } else {
        return <></>
    }
}