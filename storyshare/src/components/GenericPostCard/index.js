import React from 'react';
import {Link, withRouter} from 'react-router-dom'
import './index.scss';
import moment from 'moment';
import PostCardOptions from "./PostCardOptions";

// TODO add custom video poster
const GenericPostCard = ({post}) => {
    const fullName = (post.user && post.user.first_name) ? `${post.user.first_name} ${post.user.last_name}` : 'John Doe';
    const created = moment(post.created).fromNow();
    const numLikes = post ? post.like_count : 0;
    const likesText = (numLikes === 1) ? `${numLikes} Like` : `${numLikes} Likes`;
    return (
        <div className='column is-half-tablet is-one-third-desktop'>
            <div className="card generic-post-card">

                <Link to={`/play/${post.id}`}>
                    <div className="card-image">
                        <figure className="image is-2by1 figure-overwrite">
                            <img src={`https://picsum.photos/id/${post.id}/640/360`} alt="video poster"/>
                        </figure>
                    </div>
                </Link>
                <div className="card-content">
                    <div className="media">
                        <div className="media-content">
                            <p className="title is-6">{post.title}</p>
                            <PostCardOptions post={post}/>
                            <Link to='/users'><div className='user-name'>{fullName}</div></Link>
                            <p className='post-statistics'>
                                <span>{likesText}</span>
                                <span className='mdot'> </span>
                                <span>{created}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default withRouter(GenericPostCard);



