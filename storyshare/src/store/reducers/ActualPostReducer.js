import { GET_ONE_POST } from "../types";

// video id hardcoded to the url of some random video
const initial = {}


export const ActualPostReducer = (post=initial, action) => {
    switch(action.type) {
        case GET_ONE_POST:
            return {...post, ...action.payload};
        default:
            return post;
    }
};


