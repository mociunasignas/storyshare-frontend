import {EDITPROFILE} from "../types";

const initialState = {};

export const profileEditReducer = (state = initialState, action ) => {
    switch (action.type) {
        case EDITPROFILE: {
            return {...state, edit:action.payload}
        }
        default:
            return state
    }
};