import {EDITPOST} from "../types";

const initialState = {};

export const postEditReducer = (state = initialState, action ) => {
    switch (action.type) {
        case EDITPOST: {
            return {...state, postedit:action.payload}
        }
        default:
            return state
    }
};