import {combineReducers} from "redux";
import {loginReducer} from "./loginReducer";
import {postReducer} from "./postReducer";
import {userReducer} from './userReducer'
import {followersReducer} from './followersReducer'
import {followingReducer} from './followingReducer'
import {paginatorReducer} from './paginatorReducer'
import {profileReducer} from './profileReducer'
import {categoryReducer} from './categoryReducer'
import {profileEditReducer} from './profileEditReducer'
import {myPostReducer} from "./myPostReducer";
import {postEditReducer} from "./postEditReducer";
import {currentTabReducer} from "./currentTabReducer";
import {myFeedReducer} from "./myFeedReducer";
import {currentSearchReducer} from "./currentSearchReducer";
import {postReplyIdReducer} from "./postReplyIdReducer";
import {replyReducer} from "./replyReducer";
import {CreatePostModalReducer} from './CreatePostModalReducer'
import {ShareModalReducer} from './shareModalReducer'
import {ActualPostReducer} from './ActualPostReducer'
import {searchPostReducer} from "./searchPostReducer";
import {searchUserReducer} from "./searchUserReducer";

export const reducers = combineReducers({
    loginReducer,
    postReducer,
    userReducer,
    followersReducer,
    followingReducer,
    profileReducer,
    categoryReducer,
    paginatorReducer,
    myPostReducer,
    myFeedReducer,
    postReplyIdReducer,
    replyReducer,
    profileEditReducer,
    currentTabReducer,
    postEditReducer,
    currentSearchReducer,
    searchPostReducer,
    searchUserReducer,
    CreatePostModalReducer,
    ShareModalReducer,
    ActualPostReducer,
});
