import { SAVE_POSTS } from "../types";

// video id hardcoded to the url of some random video
const initial = {}


export const postReducer = (posts=initial, action) => {
    switch(action.type) {
        case SAVE_POSTS:
            return {...posts, ...action.payload.posts};
        default:
            return posts;
    }
};


