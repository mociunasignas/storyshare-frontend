import {CURRENTSEARCH} from "../types";

const initialState = {};

export const currentSearchReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENTSEARCH: {
            return {...state, search: action.payload}
        }
        default:
            return state
    }
};