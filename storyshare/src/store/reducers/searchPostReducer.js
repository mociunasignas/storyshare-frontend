import { SEARCHPOSTS } from "../types";

const initial = {};

export const searchPostReducer = (state=initial, action) => {
    switch(action.type) {
        case SEARCHPOSTS:
            return {...state, ...action.payload};
        default:
            return state;
    }
};
