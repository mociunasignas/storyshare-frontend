import {UPLOADPROFILE} from "../types";

const initialState = {};

export const profileReducer = (state = initialState, action ) => {
    switch (action.type) {
        case UPLOADPROFILE: {
            return {...state, profile:action.payload}
        }
        default:
            return state
    }
};