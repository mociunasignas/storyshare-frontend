import { FOLLOWERS } from "../types";

const initial = {}

export const followersReducer = (state=initial, action) => {
    switch(action.type) {
        case FOLLOWERS:
            return {...state.followers, ...action.payload};
        default:
            return state;
    }
};


