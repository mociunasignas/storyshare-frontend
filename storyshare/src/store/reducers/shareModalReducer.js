import { SHARE } from "../types";

const initial = {modalShare: false}

export const ShareModalReducer = (state=initial, action) => {
    switch(action.type) {
        case SHARE:
            return {...state, modalShare: action.payload};
        default:
            return state;
    }
};


