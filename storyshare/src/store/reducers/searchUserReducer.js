import { SEARCHUSERS } from "../types";

const initial = {};

export const searchUserReducer = (state=initial, action) => {
    switch(action.type) {
        case SEARCHUSERS:
            return {...state, ...action.payload};
        default:
            return state;
    }
};
