import { FOLLOWING } from "../types";

const initial = {}

export const followingReducer = (state=initial, action) => {
    switch(action.type) {
        case FOLLOWING:
            return {...state.following, ...action.payload};
        default:
            return state;
    }
};


