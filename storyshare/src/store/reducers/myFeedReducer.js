import {SAVEMYFEED} from "../types";

const initial = {};

export const myFeedReducer = (state = initial, action) => {
    switch (action.type) {
        case SAVEMYFEED:
            return {...initial, ...action.payload};
        default:
            return state;
    }
};
