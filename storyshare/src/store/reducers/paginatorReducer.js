import { SET_POSTS_CURRENT_PAGE } from "../types";


const initial = {
    latestPosts: {
        current: 1,
        next: null,
        prev: null,
        total: 0,
        byId: [],  // used to order all posts
        byIdCurrent: []  // used to order the posts of the current page only (not used)
    },
    // Andi: added users and followers just as a reminder, no need to implement this madness for them as well now :D
    // users: {
    //     current: 1,
    //     next: null,
    //     prev: null,
    //     total: 0,
    //     byId: []  // maybe can be used to sort users by name ?
    // },
    // followers: {
    //     current: 1,
    //     next: null,
    //     prev: null,
    //     total: 0,
    //     byId: []  // maybe can be used to sort users by name ?
    // },
    // following: {
    //     current: 1,
    //     next: null,
    //     prev: null,
    //     total: 0,
    //     byId: []  // maybe can be used to sort users by name ?
    // }
};

// this paginatorReducer could be used for multiple tables (posts, users, followers etc) because the data is namespaced
export const paginatorReducer = (data=initial, action) => {
    switch(action.type) {
        case SET_POSTS_CURRENT_PAGE:
            return ({ ...data, latestPosts: { ...data.latestPosts, ...action.payload.data } });  // changing data in namespace "latestPosts"
        default:
            return data;
    }
};
