import { SAVE_POST_REPLY_IDS } from "../types";

const initial = {};


// this reducer stores a single hashmap where the key is the "parent post id" and the value
// is an array of all reply ids: { 13: [45, 65, 75], 70: [75, 89, 101] }
export const postReplyIdReducer = (postReplyIds=initial, action) => {
    switch(action.type) {
        case SAVE_POST_REPLY_IDS:
            return {...postReplyIds, ...action.payload.postReplyIds};
        default:
            return postReplyIds;
    }
};
