import {CURRENTTAB} from "../types";

const initialState = {currentTab: "My Posts"};

export const currentTabReducer = (state = initialState, action) => {
    switch (action.type) {
        case CURRENTTAB: {
            return {...state, currentTab: action.payload}
        }
        default:
            return state
    }
};