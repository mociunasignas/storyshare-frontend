import { CATEGORIES } from "../types";

const initial = {}

export const categoryReducer = (state=initial, action) => {
    switch(action.type) {
        case CATEGORIES:
            return {...state, categories: action.payload};
        default:
            return state;
    }
};


