import {SAVEMYPOSTS} from "../types";

const initial = {};

export const myPostReducer = (state=initial, action) => {
    switch(action.type) {
        case SAVEMYPOSTS:
            return {...initial, ...action.payload};
        default:
            return state;
    }
};
