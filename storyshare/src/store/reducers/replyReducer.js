import { SAVE_REPLIES } from "../types";

const initial = {
};


export const replyReducer = (replies=initial, action) => {
    switch(action.type) {
        case SAVE_REPLIES:
            return {...replies, ...action.payload.replies};
        default:
            return replies;
    }
};
