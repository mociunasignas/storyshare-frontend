import { USERS } from "../types";

const initial = {}

export const userReducer = (state=initial, action) => {
    switch(action.type) {
        case USERS:
            return {...state.users, ...action.payload};
        default:
            return state;
    }
};


