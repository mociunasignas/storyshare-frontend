import { INIT_LIKE_IDS, LIKE_ID_MODIFY } from '../types';


const likedPostIdReducer = (likedPostIds={}, action) => {
    switch(action.type) {

        // executed when fetching /posts/liked/
        case INIT_LIKE_IDS:
            console.log(action.payload);
            return action.payload.likedPostIds;
        // triggered when changing like state
        case LIKE_ID_MODIFY:
            console.log(`modified like state of postId: ${action.payload.postId} to ${action.payload.status}`);
            const temp = {...likedPostIds};
            temp[action.payload.postId] = action.payload.status;
            return temp;
        default:
            return likedPostIds;
    }
};