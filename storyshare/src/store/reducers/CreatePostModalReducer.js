import { CREATEPOSTMODAL, CREATERESPONDMODAL } from "../types";

const initial = {
    modalPost: false,
    modalRespond: false}

export const CreatePostModalReducer = (state=initial, action) => {
    switch(action.type) {
        case CREATEPOSTMODAL:
            return {...state, modalPost: action.payload};
        case CREATERESPONDMODAL:
            return {...state, modalRespond: action.payload};
        default:
            return state;
    }
};


