export const ADD_TOKENS = 'add_tokens';
export const REFRESH_TOKEN = 'refresh_token';
export const ME = 'me';
export const LOGIN = "login";
export const ERROR = "error";
export const LOGOUT = "logout";
export const REGISTER = "register";
export const USERS = "users";
export const REFRESH = 'refresh';
export const COMMENTS = 'comments';
export const SAVE_POSTS = 'save_posts';
export const LIKE_POST = 'like-posts';
export const SAVE_REPLIES = 'save_replies';
export const SAVE_POST_REPLY_IDS = 'save_post_reply_ids';
export const GET_ONE_POST = 'get_one_post';
export const FOLLOWERS = 'followers';
export const FOLLOWING = 'following';
export const SET_POSTS_CURRENT_PAGE = 'set_posts_current_page';
export const CATEGORIES = 'categories'
export const STATUS = ' status'
export const CREATEPOSTMODAL = 'createpostmodal'
export const CREATERESPONDMODAL = 'createrespondmodal'
export const SHARE = 'share'


//ignas
export const UPLOADPROFILE = "uploadProfile";
export const EDITPROFILE = "editProfile";
export const UPDATEPROFILE = "updateProfile";
export const SAVEMYPOSTS = 'saveMyPosts';
export const EDITPOST = 'editPost';
export const UPDATEPOST = 'updatePost';
export const CURRENTTAB = 'currentTab';
export const CURRENTSEARCH = 'currentSearch';
export const SAVEMYFEED = 'saveMyFee';
export const SEARCHPOSTS = 'searchPosts';
export const SEARCHUSERS = 'searchUsers';
export const FOLLOWUSER = 'followUser';