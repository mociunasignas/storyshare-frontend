import axios from "axios";
import { SAVE_POSTS } from "../types";
import { arrToObj } from "../../helpers";
import { URL } from '../../constants';
import {setPostsCurrentPageAction} from './paginatorActions';

export const savePosts = posts => ({
    type: SAVE_POSTS,
    payload: { posts }
});


export const savePostsAction = () => async (dispatch, getState) => {
    try {
        const currentPage = getState().paginatorReducer.latestPosts.current;
        const res = await axios.get(`${URL}feed/?page=${currentPage}`);

        const fetchedPosts = arrToObj(res.data.results);
        const postsAlreadyInRedux = getState().postReducer;
        const combinedPosts =  {...postsAlreadyInRedux, ...fetchedPosts};

        const byIdCurrent = Object.keys(fetchedPosts).sort((a, b) => b-a);
        const byId = Object.keys(combinedPosts).sort((a, b) => b-a);

        // TODO compare total and res.data.count.
        //  If total not the same, make additional fetches. (if difference of total <10 only fetch page1)
        //  this is required to see newly created posts and still be able to cache already fetched posts
        const paginationData = {
            // current: currentPage,
            next: res.data.next,
            prev: res.data.previous,
            total: res.data.count,
            isLoaded: true,
            byIdCurrent: byIdCurrent,
            byId: byId
        };
        dispatch(setPostsCurrentPageAction(paginationData));

        dispatch(savePosts(fetchedPosts));
        return res;
    } catch (e) {
        // commented out error dispatch as it would log you out and redirect you
        // dispatch(error("something went wrong"));
        console.error(e);
    }
};

