import axios from "axios";
import {SEARCHUSERS} from "../types";
import { URL } from '../../constants';

export const searchUsers = search_users => ({
    type: SEARCHUSERS,
    payload: { search_users }
});

export const searchUsersAction = (props) => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    // eslint-disable-next-line
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    try {
        // console.log('FETCHING POSTS URL:', `${URL}users/?search=${props.search}`, config);
        const res = await axios.get(`${URL}users/?search=${props.search}`);
        dispatch(searchUsers(res.data.results));
        return res.data.results
    } catch (e) {
        // commented out error dispatch as it would log you out and redirect you
        // dispatch(error("something went wrong"));
    }
};
