import {SHARE} from "../types";

export const shareModal = data => {
    return {
        type: SHARE,
        payload: data
    }
};
