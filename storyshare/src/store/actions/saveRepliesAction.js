import axios from "axios";
import { SAVE_REPLIES, SAVE_POST_REPLY_IDS } from "../types";
import { arrToObj } from "../../helpers";
import { URL } from '../../constants';

/**
 * action to store new replies in redux.
 * Make sure the replies array (that is returned from the backend) is first transformed with "arrToObj" helper function!
 * @param {Object} replies an object where the key is the id of the reply
 */
const saveReplies = replies => ({
    type: SAVE_REPLIES,
    payload: { replies }
});


/**
 * action to store the ids of all replies to a post in redux
 * @param {Object} replies an object where the key is the post id and the value is an array of reply ids
 */
const savePostReplyIds = postReplyIds => ({
    type: SAVE_POST_REPLY_IDS,
    payload: { postReplyIds }
});


export const saveRepliesAction = (postId) => async (dispatch, getState) => {
    try {
        const res = await axios.get(`${URL}replies/${postId}/`);

        const fetchedReplies = arrToObj(res.data);
        await dispatch(saveReplies(fetchedReplies));

        const postReplyIds = Object.keys(fetchedReplies).sort((a, b) => a-b);  // TODO consider flipping a-b
        await dispatch(savePostReplyIds({ [postId]: postReplyIds }));

        return res;
    } catch (e) {
        console.error(e);
    }
};
