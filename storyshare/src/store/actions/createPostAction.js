import {CREATEPOSTMODAL} from "../types";

export const createPostModal = data => {
    return {
        type: CREATEPOSTMODAL,
        payload: data
    }
};
