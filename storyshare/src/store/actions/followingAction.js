import axios from "axios";
import {FOLLOWING} from "../types";
import {URL} from '../../constants';

export const getFollowing = data => {
    return {
        type: FOLLOWING,
        payload: {data}
    };
};

export const followingAction = () => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };
    const response = await axios.get(`${URL}users/following/`, config);
    const data = response.data.results;
    dispatch(getFollowing(data));
};
