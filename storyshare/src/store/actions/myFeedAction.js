import axios from "axios";
import {SAVEMYFEED} from "../types";
import {URL} from '../../constants';

export const saveMyFeed = my_feed => ({
    type: SAVEMYFEED,
    payload: {my_feed}
});

export const myFeedAction = () => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    try {
        // console.log('FETCHING MY FEED URL:', `${URL}feed/followees/`);
        const feedresponse = await axios.get(`${URL}feed/followees/`, config);
        dispatch(saveMyFeed(feedresponse.data.results));
        return feedresponse.data.results
    } catch (e) {
        // commented out error dispatch as it would log you out and redirect you
        // dispatch(error("something went wrong"));
    }
};