import axios from "axios";
import { SEARCHPOSTS } from "../types";
import { URL } from '../../constants';

export const searchPosts = search_posts => ({
    type: SEARCHPOSTS,
    payload: { search_posts }
});

export const searchPostsAction = (props) => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    // eslint-disable-next-line
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    try {
        // console.log('FETCHING POSTS URL:', `${URL}feed/?search=${props.search}`, config);
        const res = await axios.get(`${URL}feed/?search=${props.search}`);
        dispatch(searchPosts(res.data.results));
        return res.data.results
    } catch (e) {
        // commented out error dispatch as it would log you out and redirect you
        // dispatch(error("something went wrong"));
    }
};
