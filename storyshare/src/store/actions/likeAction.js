// import { LIKE_POST } from "../types";
// import { refreshAccessToken } from "./loginActions";
// import { editPostAction } from "./postActions";
import { URL } from '../../constants'
import axios from "axios";

//const likePost = (post) => {
//    return {
//        type: LIKE_POST,
//        payload: { post }
//    }
//};

export const likeAPostAction = (postId) => async (dispatch, getState) => {
    const token = getState().loginReducer.token;

    try {
        const config = { headers: { Authorization: `Bearer ${token}`} };
        const res = await axios.post(`${URL}posts/like/${postId}/`, {},  config);
        const alreadyLiked = res.status === 200;

        if (alreadyLiked) {
            console.log("Oh, it seems you already liked this post. Let me unlike it for you.");
            // const res = await axios.delete(`${URL}posts/like/${postId}/`, {},  config); //TODO find out why throwing error
        }
    } catch(e) {
        console.error(e);
    }
};






        // worked
        //   const config = {
        //     method: (alreadyLiked ? 'DELETE' : 'POST'),
        //     headers: {
        //         "Content-type": 'application/json',
        //         "Authorization": `Bearer ${token}`
        //     }
        // };

        // const res = await fetch(`${URL}posts/like/${postId}/`, config);
        // const data = await res.json();
        // console.log('data', res);


        //worked
        // const config = {
        //     headers: {
        //       Authorization: `Bearer ${token}`
        //     }
        //   };
        //   const res = await axios.post(`${URL}posts/like/${postId}/`, {},  config);
        //   const data = res.data;
        // console.log('data', res);