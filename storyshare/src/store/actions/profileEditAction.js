import {EDITPROFILE} from "../types";

export const editProfile = response => {
    return {
        type: EDITPROFILE,
        payload: true
    }
};

export const noEditProfile = response => {
    return {
        type: EDITPROFILE,
        payload: false
    }
};

export const profileEditAction = () => (dispatch, getState) => {
    dispatch(editProfile());
};

export const profileNoEditAction = () => (dispatch, getState) => {
    dispatch(noEditProfile());
};