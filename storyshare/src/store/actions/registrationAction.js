import axios from "axios";
import { URL } from '../../constants';
// import { ERROR, LOGIN, LOGOUT } from "../types";

// export const register1 = (token, refresh) => {
//   return {
//     type: LOGIN,
//     token: token,
//     refresh: refresh
//   };
// };

// const error = error => {
//   return {
//     type: ERROR,
//     payload: error
//   };
// };

// export const logout = () => {
//   return {
//     type: LOGOUT
//   };
// };

export const requestValidationCode = (email) => async dispatch => {
    // console.log(email);
    try {
        const res = await axios.post(`${URL}registration/`, { email });
        // console.log("response", res);
        return res;
    } catch (e) {
        // dispatch(error("something went wrong"));
        console.log("something went wrong");
    }
};
