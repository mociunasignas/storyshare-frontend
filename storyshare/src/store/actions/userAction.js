import axios from "axios";
import { USERS } from "../types";
import { URL } from '../../constants';

export const users = data => {
  return {
    type: USERS,
    payload: data
  };
};

export const getUsers = () => async (dispatch, getState) => {
  const token = getState().loginReducer.token;
  const config = {
    headers: {
      Authorization: `Bearer ${token}`
    }
  };
  const response = await axios.get(`${URL}users/`, config);
  const data = response.data.results;
  dispatch(users(data));
};
