import axios from "axios";
import {UPDATEPROFILE} from "../types";
import {URL} from '../../constants';

export const uploadProfile = response => {
    return {
        type: UPDATEPROFILE,
        payload: response
    }
};

export const profileUpdateAction = (content) => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    const data = {
        // username:content.username,
        // email:content.email,
        first_name:content.first_name,
        last_name:content.last_name,
        profile: {
            about: content.about,
            location: content.location,
            phone: content.phone
        },
    };

    const response = await axios.patch(
        `${URL}me/`,
        data,
        config);
    dispatch(uploadProfile(response.data));
    return response
};