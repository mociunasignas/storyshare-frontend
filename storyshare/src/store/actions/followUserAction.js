import {URL} from '../../constants'
import axios from "axios";

export const followUserAction = (id) => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {headers: {Authorization: `Bearer ${token}`}};

    try {
        const res = await axios.post(`${URL}users/follow/${id}/`, {}, config);
        const alreadyLiked = res.status === 200;

        if (alreadyLiked) {
            console.log("Oh, it seems you already liked this post. Let me unlike it for you.");
            // eslint-disable-next-line
            const res = await axios.delete(`${URL}users/follow/${id}/`,  config);
        }
        return res.status
    } catch (e) {
        console.error(e);
    }
};
