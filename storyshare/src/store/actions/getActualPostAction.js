import axios from "axios";
import {GET_ONE_POST} from "../types";
import {URL} from '../../constants';

export const getPost = post => {
    return {
        type: GET_ONE_POST,
        payload: post,
    };
};

export const getActualPostAction = (postID) => async (dispatch, getState) => {
    try {
        const token = getState().loginReducer.token;
        const config = {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
        // const response = await axios.get('https://api.test.strav.us/categories/', config);
        const response = await axios.get(`${URL}posts/${postID}/`, config);
        const data = response.data
        dispatch(getPost(data));
        return response.data;
    } catch (error) {
        console.log('error', error)
    }
}
