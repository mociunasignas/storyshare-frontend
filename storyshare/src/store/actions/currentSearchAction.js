import {CURRENTSEARCH} from "../types";

export const currentSearch = search => {
    return {
        type: CURRENTSEARCH,
        payload: search
    }
};

export const currentSearchAction = (search) => (dispatch, getState) => {
    dispatch(currentSearch(search));
};