import {CURRENTTAB} from "../types";

export const currentTab = tab => {
    return {
        type: CURRENTTAB,
        payload: tab
    }
};

export const currentTabAction = (tab) => (dispatch, getState) => {
    dispatch(currentTab(tab));
};