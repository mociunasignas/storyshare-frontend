import axios from "axios";
import { SAVEMYPOSTS } from "../types";
import { URL } from '../../constants';

export const saveMyPosts = my_posts => ({
    type: SAVEMYPOSTS,
    payload: { my_posts }
});

export const myPostsAction = () => async (dispatch, getState) => {
    // console.log("in my posts action")
    const my_ID = getState().profileReducer.profile.id

    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    try {
        console.log('FETCHING POSTS URL:', `${URL}feed/${my_ID}/`, config);
        const res = await axios.get(`${URL}feed/${my_ID}/`);
        // console.log("myposts data results", res.data)
        dispatch(saveMyPosts(res.data));
        return res.data
    } catch (e) {
        // commented out error dispatch as it would log you out and redirect you
        // dispatch(error("something went wrong"));
    }
};
