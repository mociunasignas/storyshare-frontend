import axios from "axios";
import {CATEGORIES} from "../types";
import {URL} from '../../constants';

export const categories = data => {
    return {
        type: CATEGORIES,
        payload: data,
    };
};

export const getCategories = () => async (dispatch, getState) => {
    try {
        const token = getState().loginReducer.token;
        const config = {
            headers: {
                Authorization: `Bearer ${token}`
            }
        };
        // const response = await axios.get('https://api.test.strav.us/categories/', config);
        const response = await axios.get(`${URL}categories/`, config);
        const data = response.data.results
        dispatch(categories(data));
    } catch (error) {
        console.log('error', error)
    }
}
