import axios from "axios";
import {UPDATEPOST} from "../types";
import {URL} from '../../constants';

export const updatePost = response => {
    return {
        type: UPDATEPOST,
        payload: response
    }
};

export const postUpdateAction = (content, state) => async (dispatch, getState) => {
    // console.log("content-cats",content.categories)
    // console.log("state-cat",state.category)
    // console.log("in post update action", getState())
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    const cat_ID = content.categories.filter(cat=>cat.category_name===state.category)[0]
    // console.log("cat ID:",cat_ID)

    const data = {
        category: cat_ID,
        title:state.title,
        status:state.status,
        tags:[]

    };
    // console.log("data of post update",data);

    const response = await axios.patch(
        `${URL}posts/5/`,
        data,
        config);
    dispatch(updatePost(response.data));
    return response
};