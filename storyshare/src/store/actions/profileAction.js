import axios from "axios";
import {UPLOADPROFILE} from "../types";
import {URL} from '../../constants';

export const uploadProfile = response => {
    return {
        type: UPLOADPROFILE,
        payload: response
    }
};

export const profileAction = () => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };

    const response = await axios.get(`${URL}me/`, config);
    dispatch(uploadProfile(response.data));
    return response
};