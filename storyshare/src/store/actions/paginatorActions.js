import { SET_POSTS_CURRENT_PAGE } from "../types";


const setPostsCurrentPage = data => ({
    type: SET_POSTS_CURRENT_PAGE,
    payload: { data }
});


export const setPostsCurrentPageAction = (data) => async (dispatch, getState) => {
    dispatch(setPostsCurrentPage(data));
};
