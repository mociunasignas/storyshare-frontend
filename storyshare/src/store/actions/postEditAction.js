import {EDITPOST} from "../types";

export const editPost = response => {
    return {
        type: EDITPOST,
        payload: true
    }
};

export const noEditPost = response => {
    return {
        type: EDITPOST,
        payload: false
    }
};

export const postEditAction = () => (dispatch, getState) => {
    dispatch(editPost());
};

export const postNoEditAction = () => (dispatch, getState) => {
    dispatch(noEditPost());
};