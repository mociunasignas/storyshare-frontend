import axios from "axios";
import {FOLLOWERS} from "../types";
import {URL} from '../../constants';

export const getFollowers = data => {
    return {
        type: FOLLOWERS,
        payload: {data}
    };
};

export const followersAction = () => async (dispatch, getState) => {
    const token = getState().loginReducer.token;
    const config = {
        headers: {
            Authorization: `Bearer ${token}`
        }
    };
    const response = await axios.get(`${URL}users/followers/`, config);
    const data = response.data.results;
    dispatch(getFollowers(data));
};