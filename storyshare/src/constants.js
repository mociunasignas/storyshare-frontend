// url of the backend api
export const URL = process.env.REACT_APP_API_URL || `https://api.test.strav.us/`;
// export const URL = process.env.REACT_APP_API_URL || `http://localhost:8090/`;

// base url to the CDN provider hosting the videos
export const videoURL = process.env.REACT_APP_VIDEO_URL || 'https://storry-share.fra1.digitaloceanspaces.com/media/uploads/videos/';

// base url to the CDN hosting the thumbnails of the videos
export const posterURL = process.env.REACT_APP_VIDEO_POSTER_URL || 'https://storry-share.fra1.digitaloceanspaces.com/media/uploads/thumbnails/';


// IMPORTANT: make sure the URL always ends with a slash! (will break all ajax requests otherwise)
