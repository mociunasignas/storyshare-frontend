FROM node:alpine

RUN apk update
RUN apk add nginx
RUN apk add shadow bash
RUN apk add vim

# -- Add the nginx files

# Delete default config

# Create folder for PID file
RUN mkdir -p /run/nginx
RUN mkdir /frontend_storyshare


# -- Create /code/frontend directory and add /frontend to it.
COPY ./storyshare/package.json /frontend_storyshare/package.json
COPY ./storyshare/public /frontend_storyshare/public
COPY ./storyshare/src /frontend_storyshare/src

WORKDIR /frontend_storyshare

RUN npm update

# -- Add frontend React app.
COPY ./storyshare /frontend_storyshare

# -- Add our node_modules to $PATH.
COPY ./scripts/run.sh /frontend_storyshare/run.sh
RUN chmod +x run.sh
RUN ls -la

EXPOSE 80

CMD ["/frontend_storyshare/run.sh"]