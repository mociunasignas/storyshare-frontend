#!/usr/bin/env bash

echo "===> Building ..."
npm run build 

echo "===> Running ... "
nginx -g 'daemon off;'